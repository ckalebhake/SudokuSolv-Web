package ckh.puzzle.solver.sudokusolvweb.bean;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.DbSudoku;
import org.junit.Test;

import static com.google.code.beanmatchers.BeanMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class DbSudokuTest
{
    @Test
    public void DbSudokuIsValidBean()
    {
        assertThat(DbSudoku.class, hasValidBeanConstructor());
        assertThat(DbSudoku.class, hasValidGettersAndSetters());
    }

    //TODO: this needs tests for .toString()
}