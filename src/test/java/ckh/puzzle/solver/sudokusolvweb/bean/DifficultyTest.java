package ckh.puzzle.solver.sudokusolvweb.bean;

import ckh.puzzle.solver.sudokusolvweb.common.Difficulty;
import org.junit.Test;

import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanConstructor;
import static com.google.code.beanmatchers.BeanMatchers.hasValidBeanToString;
import static com.google.code.beanmatchers.BeanMatchers.hasValidGettersAndSetters;
import static org.junit.Assert.*;

public class DifficultyTest
{
    @Test
    public void DifficultyIsValidBean()
    {
        assertThat(Difficulty.class, hasValidBeanConstructor());
        assertThat(Difficulty.class, hasValidGettersAndSetters());
        assertThat(Difficulty.class, hasValidBeanToString());
    }
}