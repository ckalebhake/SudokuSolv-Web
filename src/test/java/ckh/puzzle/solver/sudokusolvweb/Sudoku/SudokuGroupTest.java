package ckh.puzzle.solver.sudokusolvweb.Sudoku;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuGroup;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SudokuGroupTest
{
    private SudokuGroup validGroup;
    private SudokuGroup invalidGroup;
    private SudokuGroup completeGroup;
    private SudokuGroup completeInvalidGroup;

    @Before
    public void setUp() throws Exception
    {
        validGroup = new SudokuGroup(new DigitCell[]
                {
                        new DigitCell((byte)1), new DigitCell(), new DigitCell(),
                        new DigitCell(), new DigitCell((byte)5), new DigitCell(),
                        new DigitCell(), new DigitCell(), new DigitCell((byte)9)
                });

        for (DigitCell c : validGroup.getCells())
        {
            if (!c.hasValue())
            {
                c.setCandidate((byte)1, false);
                c.setCandidate((byte)5, false);
                c.setCandidate((byte)9, false);
            }
        }

        invalidGroup = new SudokuGroup(new DigitCell[]
                {
                        new DigitCell((byte)5), new DigitCell(), new DigitCell(),
                        new DigitCell(), new DigitCell((byte)5), new DigitCell(),
                        new DigitCell(), new DigitCell(), new DigitCell((byte)9)
                });

        completeGroup = new SudokuGroup(new DigitCell[]
                {
                        new DigitCell((byte)1), new DigitCell((byte)2), new DigitCell((byte)3),
                        new DigitCell((byte)4), new DigitCell((byte)5), new DigitCell((byte)6),
                        new DigitCell((byte)7), new DigitCell((byte)8), new DigitCell((byte)9)
                });

        completeInvalidGroup = new SudokuGroup(new DigitCell[]
                {
                        new DigitCell((byte)1), new DigitCell((byte)5), new DigitCell((byte)3),
                        new DigitCell((byte)4), new DigitCell((byte)5), new DigitCell((byte)6),
                        new DigitCell((byte)7), new DigitCell((byte)5), new DigitCell((byte)9)
                });
    }

    @Test (expected = IllegalArgumentException.class)
    public void SudokuGroupTooFewCells()
    {
        new SudokuGroup(new DigitCell[]{new DigitCell(), new DigitCell()});
    }

    @Test (expected = IllegalArgumentException.class)
    public void SudokuGroupTooManyCells()
    {
        new SudokuGroup(new DigitCell[]
                {
                        new DigitCell((byte)1), new DigitCell((byte)5), new DigitCell((byte)3),
                        new DigitCell((byte)4), new DigitCell((byte)5), new DigitCell((byte)6),
                        new DigitCell((byte)7), new DigitCell((byte)5), new DigitCell((byte)9),
                        new DigitCell((byte)1)
                });
    }

    @Test (expected = IllegalArgumentException.class)
    public void SudokuGroupNullCells()
    {
        new SudokuGroup(null);
    }

    @Test
    public void groupCandidates()
    {
        Byte[] result = validGroup.groupPossibleCandidates();
        assertArrayEquals(new Byte[] {2,3,4,6,7,8}, result);
    }

    @Test
    public void groupLength()
    {
        assertEquals(9, validGroup.groupLength());
    }

    @Test
    public void getCells()
    {
        assertEquals(9, validGroup.getCells().length);
    }

    @Test
    public void isValid()
    {
        assertTrue(validGroup.isValid());
        assertFalse(invalidGroup.isValid());
        assertTrue(completeGroup.isValid());
        assertFalse(completeInvalidGroup.isValid());
    }

    @Test
    public void isComplete()
    {
        assertFalse(validGroup.isComplete());
        assertFalse(invalidGroup.isComplete());
        assertTrue(completeGroup.isComplete());
        assertTrue(completeInvalidGroup.isComplete());
    }
}