package ckh.puzzle.solver.sudokusolvweb.Sudoku;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuBoard;
import ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class SudokuSolverTest
{
    @Autowired
    SudokuSolver solver;

    @Before
    public void setUp() throws Exception
    {
        //ensure these tests show full debug output
        SudokuSolvConfig.setDebugSolvers(true);
        SudokuSolvConfig.setPrintOnSolve(true);
    }

    @Test
    public void shouldSolveSwEasiest()
    {
        //puzzle source: http://www.sudokuwiki.org/sudoku.htm ("Easiest" from the drop box towards the left)
        assertTrue(solver.solve("000105000140000670080002400063070010900000003010090520007200080026000035000409000"));
    }

    @Test
    public void shouldSolveSwEasy17()
    {
        //puzzle source: http://www.sudokuwiki.org/sudoku.htm ("Easy 17" from the drop box towards the left)
        assertTrue(solver.solve("000041000060000200000000000320600000000050041700000000000200300048000000501000000"));
    }

    @Test
    public void shouldSolveSwGentle()
    {
        //puzzle source: http://www.sudokuwiki.org/sudoku.htm ("Gentle" from the drop box towards the left)
        assertTrue(solver.solve("000004028406000005100030600000301000087000140000709000002010003900000507670400000"));
    }

    @Test
    public void shouldSolveNakedPairsExample()
    {
        //puzzle source: http://www.sudokuwiki.org/Naked_Candidates#NP (see the "Load Example" link)
        assertTrue(solver.solve("400000938032094100095300240370609004529001673604703090957008300003900400240030709"));
    }

    //TODO: this wont pass until we implement at least:
    //naked triples
    @Ignore
    @Test
    public void shouldSolveNakedTriplesExample()
    {
        //puzzle source: http://www.sudokuwiki.org/sudoku.htm ("Naked Triples" from the drop box towards the left)
        assertTrue(solver.solve("000000000001900500560310090100600028004000700270004003040068035002005900000000000"));
    }

    @Test
    public void shouldSolveHiddenPairsExample()
    {
        //puzzle source: http://www.sudokuwiki.org/Hidden_Candidates#HP (see the "Load Example" link)
        assertTrue(solver.solve("000000000904607000076804100309701080008000300050308702007502610000403208000000000"));
    }

    //TODO: this one wont pass until we implement at least:
    //y-wings
    //simple coloring
    @Ignore
    @Test
    public void shouldSolvePointingPairsExample()
    {
        //puzzle source: http://www.sudokuwiki.org/Intersection_Removal#LBR (see the "Load Example" link)
        assertTrue(solver.solve("017903600000080000900000507072010430000402070064370250701000065000030000005601720"));
    }

    //TODO: this one wont pass until we implement at least:
    //y-wings
    @Ignore
    @Test
    public void shouldSolveBoxLineReductionExample()
    {
        //puzzle source: http://www.sudokuwiki.org/Intersection_Removal#IR (see the "Load Example" link)
        assertTrue(solver.solve("016007803090800000870001260048000300650009082039000650060900020080002936924600510"));
    }

    //TODO: this one wont pass until we implement at least:
    //y-wings
    @Ignore
    @Test
    public void shouldSolveSwModerate()
    {
        //puzzle source: http://www.sudokuwiki.org/sudoku.htm ("Moderate" from the drop box towards the left)
        assertTrue(solver.solve("400010000000309040070005009000060021004070600190050000900400070030608000000030006"));
    }
}