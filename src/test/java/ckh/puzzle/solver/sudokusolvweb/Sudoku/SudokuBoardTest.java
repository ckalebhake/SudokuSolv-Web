package ckh.puzzle.solver.sudokusolvweb.Sudoku;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuBoard;
import org.junit.Test;

import java.util.Arrays;

public class SudokuBoardTest
{
    @Test
    public void createsValidBoard()
    {
        SudokuBoard test = new SudokuBoard("meaningless characters!" +
                "100|090|000" +
                "*2*|*8*|***" +
                "..3|.7.|..." +
                "-----------" +
                "???|46?|???" +
                "000050000" +
                "000046000" +
                "000030700" +
                "000020080" +
                "000010009" );

        System.out.println("normal ToString");
        System.out.println(test);

        System.out.println("Rows: " + Arrays.toString(test.getRows()));
        System.out.println("Columns: " + Arrays.toString(test.getColumns()));
        System.out.println("Boxes: " + Arrays.toString(test.getBoxes()));

        System.out.println("debugPrint()");
        test.debugPrint();
    }

    @Test (expected = InvalidSudokuException.class)
    public void constructorNullArg()
    {
        SudokuBoard test = new SudokuBoard((String)null);
    }

    @Test (expected = InvalidSudokuException.class)
    public void constructorTooShortArg()
    {
        SudokuBoard test = new SudokuBoard("12345");
    }

    @Test (expected = InvalidSudokuException.class)
    public void constructorMeaninglessArg()
    {
        SudokuBoard test = new SudokuBoard("this string is really really really really really really really long!  at least 81 characters long, in fact!  But it's still totally meaningless!  Whoopsie!");
    }

    @Test (expected = InvalidSudokuException.class)
    public void constructorTooManyDigits()
    {
        SudokuBoard test = new SudokuBoard("7984652198746541981621198465134949824959539938465132196949845165198165414981965194919519198654196859416854198549419685419854985498541941984984984194198149418163521981651971651");
    }
}