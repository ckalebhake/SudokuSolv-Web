package ckh.puzzle.solver.sudokusolvweb.common;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuGroup;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.IllegalProposedChangesException;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ChangeProposalJustification;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChanges;
import ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class ProposedChangesTest
{
    private DigitCell[] testCells;
    private SudokuGroup testGroup;

    @Before
    public void setUp() throws Exception
    {
        testCells = (new DigitCell[]
                {
                   new DigitCell((byte)1),
                   new DigitCell(),
                   new DigitCell(),
                   new DigitCell(),
                   new DigitCell((byte)2),
                   new DigitCell(),
                   new DigitCell(),
                   new DigitCell(),
                   new DigitCell((byte)3),
                });

        testGroup = new SudokuGroup(testCells);

        //dont allow these tests to propose changes without justification
        SudokuSolvConfig.setErrorUnjustifiedChanges(true);
    }

    @Test
    public void proposeAndApply()
    {
        ProposedChanges<DigitCell, Byte> test = new ProposedChanges<>();

        test.proposeValueChange(testCells[1], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeValueChange(testCells[2], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));

        test.applyChanges();

        assertThat(testCells[1].getValue(), equalTo((byte)5));
        assertThat(testCells[2].getValue(), equalTo((byte)1));
        assertThat(test.getJustifications(), hasSize(1));
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void proposeValueConflict()
    {
        ProposedChanges<DigitCell, Byte> test = new ProposedChanges<>();

        test.proposeValueChange(testCells[1], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeValueChange(testCells[1], (byte)2, ChangeProposalJustification.becauseOf(testCells[0]));
    }

    @Test
    public void joinProposals()
    {
        ProposedChanges<DigitCell, Byte> proposalsA = new ProposedChanges<>();
        ProposedChanges<DigitCell, Byte> proposalsB = new ProposedChanges<>();
        ProposedChanges<DigitCell, Byte> proposalsC = new ProposedChanges<>();

        proposalsA.proposeValueChange(testCells[1], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeValueChange(testCells[2], (byte)5, ChangeProposalJustification.becauseOf(testCells[4]));
        proposalsC.proposeValueChange(testCells[3], (byte)6, ChangeProposalJustification.becauseOf(testCells[8]));

        proposalsB.join(proposalsC);
        proposalsA.join(proposalsB);

        proposalsA.applyChanges();

        assertThat(testCells[1].getValue(), equalTo((byte)4));
        assertThat(testCells[2].getValue(), equalTo((byte)5));
        assertThat(testCells[3].getValue(), equalTo((byte)6));
        assertThat(proposalsA.getJustifications(), hasSize(3));
    }

    @Test
    public void joinProposalsAllowsDuplicateChanges()
    {
        ProposedChanges<DigitCell, Byte> proposalsA = new ProposedChanges<>();
        ProposedChanges<DigitCell, Byte> proposalsB = new ProposedChanges<>();
        ProposedChanges<DigitCell, Byte> proposalsC = new ProposedChanges<>();

        proposalsA.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsC.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsB.join(proposalsC);
        proposalsA.join(proposalsB);

        proposalsA.applyChanges();

        assertThat(testCells[2].getValue(), equalTo((byte)4));
        assertThat(proposalsA.getJustifications(), hasSize(1));
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void joinProposalsConflicting()
    {
        ProposedChanges<DigitCell, Byte> proposalsA = new ProposedChanges<>();
        ProposedChanges<DigitCell, Byte> proposalsB = new ProposedChanges<>();

        proposalsA.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeValueChange(testCells[2], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsA.join(proposalsB);
    }

    @Test
    public void joinNull()
    {
        ProposedChanges<DigitCell, Byte> test = new ProposedChanges<>();

        test.proposeValueChange(testCells[1], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeValueChange(testCells[2], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));

        test.join(null);
        test.applyChanges();

        assertThat(testCells[1].getValue(), equalTo((byte)5));
        assertThat(testCells[2].getValue(), equalTo((byte)1));

        assertThat(test.getJustifications(), hasSize(1));
    }
}