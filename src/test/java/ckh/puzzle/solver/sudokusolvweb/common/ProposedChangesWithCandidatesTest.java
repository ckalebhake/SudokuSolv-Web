package ckh.puzzle.solver.sudokusolvweb.common;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuGroup;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.IllegalProposedChangesException;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ChangeProposalJustification;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChangesWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class ProposedChangesWithCandidatesTest
{
    private DigitCell[] testCells;
    private SudokuGroup testGroup;

    @Before
    public void setUp() throws Exception
    {
        testCells = (new DigitCell[]
                {
                        new DigitCell((byte)1),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell((byte)2),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell((byte)3),
                });

        testGroup = new SudokuGroup(testCells);

        //dont allow these tests to propose changes without justification
        SudokuSolvConfig.setErrorUnjustifiedChanges(true);
    }

    @Test
    public void proposeAndApplyValues()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> test = new ProposedChangesWithCandidates<>();

        test.proposeValueChange(testCells[1], (byte)5, ChangeProposalJustification.becauseOf(testCells[1], (byte)5));
        test.proposeValueChange(testCells[2], (byte)1, ChangeProposalJustification.becauseOf(testCells[2], (byte)1));

        test.applyChanges();

        assertThat(testCells[1].getValue(), equalTo((byte)5));
        assertThat(testCells[2].getValue(), equalTo((byte)1));
        assertThat(test.getJustifications(), hasSize(2));
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void proposeValueConflict()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> test = new ProposedChangesWithCandidates<>();

        test.proposeValueChange(testCells[1], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeValueChange(testCells[1], (byte)2, ChangeProposalJustification.becauseOf(testCells[0]));
    }

    @Test
    public void proposeAndApplyCandidates()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> test = new ProposedChangesWithCandidates<>();

        test.proposeCandidateElimination(testCells[1], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)2, ChangeProposalJustification.becauseOf(testCells[4]));
        test.proposeCandidateElimination(testCells[1], (byte)3, ChangeProposalJustification.becauseOf(testCells[8]));

        test.applyChanges();

        assertFalse(testCells[1].checkCandidate((byte)1));
        assertFalse(testCells[1].checkCandidate((byte)2));
        assertFalse(testCells[1].checkCandidate((byte)3));
        assertTrue(testCells[1].checkCandidate((byte)4));
        assertTrue(testCells[1].checkCandidate((byte)5));
        assertTrue(testCells[1].checkCandidate((byte)6));
        assertTrue(testCells[1].checkCandidate((byte)7));
        assertTrue(testCells[1].checkCandidate((byte)8));
        assertTrue(testCells[1].checkCandidate((byte)9));
        assertThat(test.getJustifications(), hasSize(3));
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void proposeIllegalCandidate()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> test = new ProposedChangesWithCandidates<>();

        test.proposeCandidateElimination(testCells[1], (byte)37, ChangeProposalJustification.becauseOf(testCells[0]));
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void proposeRemoveAllCandidates()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> test = new ProposedChangesWithCandidates<>();

        test.proposeCandidateElimination(testCells[1], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)2, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)3, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)6, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)7, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)8, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)9, ChangeProposalJustification.becauseOf(testCells[0]));
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void proposeRemoveAllCandidatesTwoSteps()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> test = new ProposedChangesWithCandidates<>();

        test.proposeCandidateElimination(testCells[1], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)2, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)3, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));

        test.applyChanges();
        test = new ProposedChangesWithCandidates<>();

        test.proposeCandidateElimination(testCells[1], (byte)6, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)7, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)8, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeCandidateElimination(testCells[1], (byte)9, ChangeProposalJustification.becauseOf(testCells[0]));
    }

    @Test
    public void joinProposals()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsA = new ProposedChangesWithCandidates<>();
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsB = new ProposedChangesWithCandidates<>();
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsC = new ProposedChangesWithCandidates<>();

        proposalsA.proposeValueChange(testCells[1], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeValueChange(testCells[2], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsC.proposeValueChange(testCells[3], (byte)6, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsA.proposeCandidateElimination(testCells[1], (byte)4, ChangeProposalJustification.becauseOf(testCells[4]));
        proposalsA.proposeCandidateElimination(testCells[2], (byte)5, ChangeProposalJustification.becauseOf(testCells[4]));
        proposalsA.proposeCandidateElimination(testCells[3], (byte)6, ChangeProposalJustification.becauseOf(testCells[8]));

        proposalsB.join(proposalsC);
        proposalsA.join(proposalsB);

        proposalsA.applyChanges();

        assertThat(testCells[1].getValue(), equalTo((byte)4));
        assertThat(testCells[2].getValue(), equalTo((byte)5));
        assertThat(testCells[3].getValue(), equalTo((byte)6));
        assertFalse(testCells[1].checkCandidate((byte)4));
        assertFalse(testCells[2].checkCandidate((byte)5));
        assertFalse(testCells[3].checkCandidate((byte)6));
        assertThat(proposalsA.getJustifications(), hasSize(3));
    }

    @Test
    public void joinProposalsAllowsDuplicateChanges()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsA = new ProposedChangesWithCandidates<>();
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsB = new ProposedChangesWithCandidates<>();
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsC = new ProposedChangesWithCandidates<>();

        proposalsC.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsA.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsB.proposeCandidateElimination(testCells[3], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsC.proposeCandidateElimination(testCells[3], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsB.join(proposalsC);
        proposalsA.join(proposalsB);

        proposalsA.applyChanges();

        assertThat(testCells[2].getValue(), equalTo((byte)4));
        assertThat(proposalsA.getJustifications(), hasSize(1));
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void joinProposalsConflictingValues()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsA = new ProposedChangesWithCandidates<>();
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsB = new ProposedChangesWithCandidates<>();

        proposalsA.proposeValueChange(testCells[2], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeValueChange(testCells[2], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsA.join(proposalsB);
    }

    @Test (expected = IllegalProposedChangesException.class)
    public void joinProposalsRemoveAllCandidates()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsA = new ProposedChangesWithCandidates<>();
        ProposedChangesWithCandidates<DigitCell, Byte> proposalsB = new ProposedChangesWithCandidates<>();

        proposalsA.proposeCandidateElimination(testCells[1], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsA.proposeCandidateElimination(testCells[1], (byte)2, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsA.proposeCandidateElimination(testCells[1], (byte)3, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsA.proposeCandidateElimination(testCells[1], (byte)4, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsA.proposeCandidateElimination(testCells[1], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsB.proposeCandidateElimination(testCells[1], (byte)6, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeCandidateElimination(testCells[1], (byte)7, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeCandidateElimination(testCells[1], (byte)8, ChangeProposalJustification.becauseOf(testCells[0]));
        proposalsB.proposeCandidateElimination(testCells[1], (byte)9, ChangeProposalJustification.becauseOf(testCells[0]));

        proposalsA.join(proposalsB);
    }

    @Test
    public void joinNull()
    {
        ProposedChangesWithCandidates<DigitCell, Byte> test = new ProposedChangesWithCandidates<>();

        test.proposeValueChange(testCells[1], (byte)5, ChangeProposalJustification.becauseOf(testCells[0]));
        test.proposeValueChange(testCells[2], (byte)1, ChangeProposalJustification.becauseOf(testCells[0]));

        test.join(null);
        test.applyChanges();

        assertThat(testCells[1].getValue(), equalTo((byte)5));
        assertThat(testCells[2].getValue(), equalTo((byte)1));
        assertThat(test.getJustifications(), hasSize(1));
    }
}