package ckh.puzzle.solver.sudokusolvweb.common;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.InvalidCandidateException;
import org.junit.Before;
import org.junit.Test;


import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class DigitCellTest
{
    private DigitCell blankCell;
    private DigitCell fixedCell;

    @Before
    public void setUp() throws Exception
    {
        blankCell = new DigitCell();
        fixedCell = new DigitCell((byte)5);
    }

    @Test
    public void validInitialState()
    {
        assertFalse(blankCell.hasValue());
        assertFalse(blankCell.isLocked());
        assertArrayEquals(blankCell.getValidCandidates(), blankCell.getCurrentCandidates());

        assertTrue(fixedCell.hasValue());
        assertTrue(fixedCell.isLocked());
        assertThat(fixedCell.getValue(), equalTo((byte)5));

        Byte[] fixedCellCurrentCandidates = fixedCell.getCurrentCandidates();
        assertThat(fixedCellCurrentCandidates.length, equalTo(1));
        assertThat(fixedCellCurrentCandidates[0], equalTo((byte)5));
    }

    @Test
    public void candidateManipulation()
    {
        assertTrue(blankCell.checkCandidate((byte)3));
        blankCell.setCandidate((byte)3, false);
        assertFalse(blankCell.checkCandidate((byte)3));
        blankCell.setCandidate((byte)3, true);
        assertTrue(blankCell.checkCandidate((byte)3));
    }

    @Test(expected = IllegalStateException.class)
    public void setCandidateLockedCell()
    {
        fixedCell.setCandidate((byte)1, true);
    }

    @Test(expected = InvalidCandidateException.class)
    public void setCandidateBadCandidate()
    {
        blankCell.setCandidate((byte) 107, true);
    }

    @Test(expected = InvalidCandidateException.class)
    public void SetCandidateNullCandidate()
    {
        blankCell.setCandidate(null, false);
    }

    @Test(expected = IllegalStateException.class)
    public void setCandidatesLockedCell()
    {
        fixedCell.setCurrentCandidates(new Byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9});
    }

    @Test(expected = InvalidCandidateException.class)
    public void setCandidatesBadCandidate()
    {
        blankCell.setCurrentCandidates(new Byte[] {107});
    }

    @Test(expected = IllegalArgumentException.class)
    public void setCandidatesNullArray()
    {
        blankCell.setCurrentCandidates(null);
    }

    @Test(expected = InvalidCandidateException.class)
    public void setCandidatesNullCandidate()
    {
        blankCell.setCurrentCandidates(new Byte[] {1, 2, null, 3, 4});
    }

    @Test(expected = InvalidCandidateException.class)
    public void checkCandidateBadCandidate()
    {
        blankCell.checkCandidate((byte)107);
    }

    @Test(expected = InvalidCandidateException.class)
    public void checkCandidateNullCandidate()
    {
        blankCell.checkCandidate(null);
    }

    @Test
    public void clear()
    {
        blankCell.setCandidate((byte)1, false);
        blankCell.setValue((byte)3);
        blankCell.clear();
        assertFalse(blankCell.hasValue());
        assertFalse(blankCell.checkCandidate((byte)1));
    }

    @Test
    public void reset()
    {
        fixedCell.unlockAndSetValue((byte)5);
        fixedCell.reset();
        assertTrue(fixedCell.isLocked());
        assertEquals(new Byte((byte)5), fixedCell.getValue());
    }

    @Test(expected = IllegalStateException.class)
    public void lockAndModify()
    {
        blankCell.lock();
        blankCell.setValue((byte)1);
    }

    @Test
    public void unlockAndModify()
    {
        blankCell.unlock();
        blankCell.setValue((byte)1);
        assertEquals(new Byte((byte)1), blankCell.getValue());

        blankCell.reset();

        blankCell.unlockAndSetValue((byte)1);
        assertEquals(new Byte((byte)1), blankCell.getValue());
    }
}