package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class ChangeProposalJustificationTest
{
    DigitCell[] testCells;

    @Before
    public void setUp() throws Exception
    {
        testCells = new DigitCell[]
                {
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                        new DigitCell(),
                };

        testCells[0].setValue((byte)1);
        testCells[1].setCandidate((byte)1, false);
        testCells[2].setCandidate((byte)1, false);
        testCells[3].setCandidate((byte)1, false);
        testCells[4].setCandidate((byte)1, false);
        testCells[5].setCandidate((byte)1, false);
        testCells[6].setCandidate((byte)1, false);
        testCells[7].setCandidate((byte)1, false);
        testCells[8].setValue((byte)9);
    }

    @Test
    public void becauseISaidSo() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseISaidSo();
        assertEquals(ChangeProposalJustification.Reason.BECAUSE_I_SAID_SO, justification.getReason());
    }

    @Test
    public void becauseOfValue() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(testCells[0]);
        assertEquals(ChangeProposalJustification.Reason.BECAUSE_OF_VALUE, justification.getReason());
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfValueCellWithoutValue() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(testCells[1]);
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfValueNull() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf((DigitCell) null);
    }

    @Test
    public void becauseOfValues() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(Arrays.asList(testCells[0], testCells[8]));
        assertEquals(ChangeProposalJustification.Reason.BECAUSE_OF_VALUES, justification.getReason());
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfValuesCellsWithoutValue() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(Arrays.asList(testCells));
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfValuesNull() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf((List<DigitCell>)null);
    }

    @Test
    public void becauseOfCandidate() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(testCells[1], (byte)5);
        assertEquals(ChangeProposalJustification.Reason.BECAUSE_OF_CANDIDATE, justification.getReason());
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfCandidateNullArg0() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(null, (byte)5);
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfCandidateNullArg1() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(testCells[1], null);
    }

    @Test
    public void becauseOfCandidates() throws Exception
    {
        HashMap<DigitCell, List<Byte>> map = new HashMap<>();
        map.put(testCells[1], Arrays.asList((byte)4, (byte)5, (byte)6));
        map.put(testCells[2], Arrays.asList((byte)4, (byte)5, (byte)6));
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(map);
        assertEquals(ChangeProposalJustification.Reason.BECAUSE_OF_CANDIDATES, justification.getReason());
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfCandidatesNull() throws Exception
    {
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf((HashMap<DigitCell, List<Byte>>)null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfCandidatesEmpty() throws Exception
    {
        HashMap<DigitCell, List<Byte>> map = new HashMap<>();
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(map);
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfCandidatesNullList() throws Exception
    {
        HashMap<DigitCell, List<Byte>> map = new HashMap<>();
        map.put(testCells[1], null);
        map.put(testCells[2], Arrays.asList((byte)4, (byte)5, (byte)6));
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(map);
    }

    @Test (expected = IllegalArgumentException.class)
    public void becauseOfCandidatesEmptyList() throws Exception
    {
        HashMap<DigitCell, List<Byte>> map = new HashMap<>();
        map.put(testCells[1], new ArrayList<>());
        map.put(testCells[2], Arrays.asList((byte)4, (byte)5, (byte)6));
        ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(map);
    }
}