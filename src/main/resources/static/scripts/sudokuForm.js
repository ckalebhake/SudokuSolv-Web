app.controller("sudokuFormController", function($rootScope)
{
    var ctrl = this;

    ctrl.sudokuInput = "";

    ctrl.importSudoku = function()
    {
        $rootScope.$broadcast("importSudoku", ctrl.sudokuInput);
    };

    ctrl.stepSudoku = function()
    {
        $rootScope.$broadcast("stepSudoku");
    };

    ctrl.solveSudoku = function()
    {
        $rootScope.$broadcast("solveSudoku");
    };
});