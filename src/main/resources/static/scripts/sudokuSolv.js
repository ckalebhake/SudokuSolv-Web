var app = angular.module("SudokuSolv", [])
    .component("sudokuBoard",
    {
        templateUrl : "sudokuBoard.html",
        controller : "sudokuBoardController"
    })
    .component("sudokuForm",
    {
        templateUrl : "sudokuForm.html",
        controller : "sudokuFormController"
    })
    .component("sudokuCell",
    {
        templateUrl : "sudokuCell.html",
        controller : "sudokuCellController",
        bindings :
            {
                cx : "<",
                cy : "<"
            }
    });
