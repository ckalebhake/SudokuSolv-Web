app.controller("sudokuCellController", function($scope, $rootScope)
{
    var ctrl = this;

    ctrl.candidates =
        [
            true, true, true,
            true, true, true,
            true, true, true
        ];

    ctrl.value = 0;

    $scope.$on("updateCell", function (event, data)
    {
        if (data.cx === ctrl.cx)
        {
            if (data.cy === ctrl.cy)
            {
                ctrl.value = $rootScope.sudoku.board[cx][cy].value;
                ctrl.candidates = $rootScope.sudoku.board[cx][cy].candidates;
            }
        }
    });

    $scope.$on("updateBoard", function ()
    {
        ctrl.value = $rootScope.sudoku.board[ctrl.cx][ctrl.cy].value;
        ctrl.candidates = $rootScope.sudoku.board[ctrl.cx][ctrl.cy].candidates;
    });

    $scope.candidateStyle = function(candidate)
    {
        var styles = [];

        styles.push("cell-candidate");

        //highlighting from previous step call, if there was one
        if ($rootScope.previousStep)
        {
            //candidate justifications
            if ($rootScope.previousStep.justifications)
            {
                if ($rootScope.previousStep.justifications.some(function(justification) //if there is some justification from the previous step
                    {
                        //that references this candidate from this cell

                        if (justification.candidateJustifications)
                        {
                            var justificationCandidates = justification.candidateJustifications["{" + ctrl.cx + ", " + ctrl.cy + "}"];
                            if(justificationCandidates)
                                return justificationCandidates.includes(candidate);
                        }

                        return false;
                    }))
                {
                    styles.push("justification-candidate");
                }
            }

            //candidate eliminations
            if ($rootScope.previousStep.candidateChanges.some(function(candidateChange) //if there is some candidate change
                {
                    return ((candidateChange.cellCoords.cx === ctrl.cx) && (candidateChange.cellCoords.cy === ctrl.cy) && //that applies to this cell
                        (candidateChange.candidatesToRemove.includes(candidate))); //and removes this candidate
                }))
            {
                styles.push("removed-candidate"); //then style accordingly
            }

            //value changes
            if ($rootScope.previousStep.valueChanges.some(function(valueChange) //if there is some value change
                {
                    return ((valueChange.cellCoords.cx === ctrl.cx) && (valueChange.cellCoords.cy === ctrl.cy) && //that applies to this cell
                        (valueChange.newValue === candidate)); //and sets this candidate as the new value
                }))
            {
                styles.push("set-value"); //then style accordingly
            }
        }

        return styles;
    }
});