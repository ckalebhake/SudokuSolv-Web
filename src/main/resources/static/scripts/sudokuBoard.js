app.controller("sudokuBoardController", function ($scope, $rootScope, $timeout, $http)
{
    var ctrl = this;

    ctrl.$onInit = function ()
    {
        //setup a blank board
        $rootScope.sudoku = {board: new Array(9)};
        for (var row = 0; row < 9; row++)
        {
            $rootScope.sudoku.board[row] =
                [
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]},
                    {value: 0, candidates: [true, true, true, true, true, true, true, true, true]}
                ];
        }

        //wait a second for everything to load, then import a default
        $timeout(function ()
        {
            // ctrl.importSudoku("000105000140000670080002400063070010900000003010090520007200080026000035000409000"); //swEasiest
            // ctrl.importSudoku("000004028406000005100030600000301000087000140000709000002010003900000507670400000"); //swGentle
            // ctrl.importSudoku("400010000000309040070005009000060021004070600190050000900400070030608000000030006"); //swModerate
            ctrl.importSudoku("400000938032094100095300240370609004529001673604703090957008300003900400240030709"); //sw naked pairs ex.
        }, 1000);
    };

    $scope.$on("importSudoku", function (event, boardString)
    {
        ctrl.importSudoku(boardString);
    });

    ctrl.importSudoku = function (boardString)
    {
        if ((!boardString) || (boardString.length < 81))
        {
            console.error("failed to import sudoku: input null or too short to possibly be valid");
            return;
        }

        var meaningfulCharacters = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '*', '.', '?'];
        var foundCharacters = new Array(81);
        var count = 0;

        //search for meaningful characters
        for (var charIndex = 0; charIndex < boardString.length; charIndex++)
        {
            var char = boardString.charAt(charIndex);

            if (meaningfulCharacters.includes(char))
            {
                if (count === 81)
                {
                    console.error("failed to import sudoku: input had too many meaningful characters for this to be a sudoku.");
                    return;
                }

                foundCharacters[count++] = char;
            }
        }

        if (count < 81)
        {
            console.error("failed to import sudoku: input did not have enough meaningful characters to be a sudoku.");
            return;
        }

        //we now have an array of 81 meaningful characters.  Fill out the board.
        for (var row = 0; row < 9; row++)
        {
            for (var col = 0; col < 9; col++)
            {
                switch (foundCharacters[(row * 9) + col])
                {
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        $rootScope.sudoku.board[row][col].fixed = true;
                        $rootScope.sudoku.board[row][col].value = foundCharacters[(row * 9) + col];
                        $rootScope.sudoku.board[row][col].candidates = [false, false, false, false, false, false, false, false, false];
                        $rootScope.sudoku.board[row][col].candidates[$rootScope.sudoku.board[row][col].value - 1] = true;
                        break;

                    default :
                        $rootScope.sudoku.board[row][col].fixed = false;
                        $rootScope.sudoku.board[row][col].value = 0;
                        break;
                }
            }
        }

        //and now tell all the cells to update
        $scope.$broadcast("updateBoard");
    };

    $scope.$on("stepSudoku", function ()
    {
        ctrl.stepSudoku();
    });

    ctrl.stepSudoku = function ()
    {
        //if there are still changes being shown from the previous iteration, apply them
        if ($rootScope.previousStep)
            $rootScope.sudoku = $rootScope.previousStep.sudokuAfterChanges;

        //remove highlighting until we have a response from the previous go
        $rootScope.previousStep = null;

        $http(
            {
                method: "POST",
                url: "/api/sudoku/step",
                data: JSON.stringify($rootScope.sudoku)
            }).then(function success(response)
        {
            $rootScope.previousStep = response.data;

            $scope.$broadcast("updateBoard");
        }, function error(response)
        {
            console.error("error: " + response.message);
        });
    };

    $scope.$on("solveSudoku", function ()
    {
        ctrl.solveSudoku();
    });

    ctrl.solveSudoku = function ()
    {
        $http(
            {
                method: "POST",
                url: "/api/sudoku/solve",
                data: JSON.stringify($rootScope.sudoku)
            }).then(function success(response)
        {
            $rootScope.sudoku = response.data;
            $rootScope.previousStep = null; //remove data about the previous step, if there was one, since we just solved the entire board
            $scope.$broadcast("updateBoard");
        }, function error(response)
        {
            console.error("error: " + response.message);
        });
    };

    $scope.cellStyle = function (row, col)
    {
        var styles = [];

        styles.push("sudoku-row-" + row);
        styles.push("sudoku-column-" + col);

        //highlighting from previous step call, if there was one
        if ($rootScope.previousStep)
        {
            //highlight value justifications
            if ($rootScope.previousStep.justifications)
            {
                if ($rootScope.previousStep.justifications.some(function (justification) //if there is some justification from the previous step
                    {
                        return justification.valueJustifications && justification.valueJustifications.some(function (valueJustification) //which has some value justication
                        {
                            return ((valueJustification.cx === row) && (valueJustification.cy === col)); //which references this cell
                        })
                    }))
                {
                    styles.push("justification-value") //then style the cell accordingly
                }
            }
        }

        return styles;
    };
});