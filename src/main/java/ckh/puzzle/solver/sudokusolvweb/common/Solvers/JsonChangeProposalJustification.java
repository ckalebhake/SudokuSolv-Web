package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellCoords;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class JsonChangeProposalJustification<CellType extends Cell<CellDataType>, CellDataType>
{
    private final ChangeProposalJustification.Reason reason;
    private final List<CellCoords> valueJustifications;
    private final Map<CellCoords, List<CellDataType>> candidateJustifications;

    public JsonChangeProposalJustification(PuzzleBoard<?, CellType, CellDataType> puzzleBoard, ChangeProposalJustification<CellType, CellDataType> justification)
    {
        this.reason = justification.getReason();

        if (justification.getValueJustifications() == null || justification.getValueJustifications().isEmpty())
        {
            valueJustifications = null;
        }
        else
        {
            valueJustifications = justification
                    .getValueJustifications()
                    .stream()
                    .map(puzzleBoard::findCell)
                    .collect(Collectors.toList());
        }

        if (justification.getCandidateJustifications() == null || justification.getCandidateJustifications().isEmpty())
        {
            candidateJustifications = null;
        }
        else
        {
            candidateJustifications = justification
                    .getCandidateJustifications()
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(entry -> puzzleBoard.findCell(entry.getKey()), Map.Entry::getValue));
        }
    }

    public ChangeProposalJustification.Reason getReason()
    {
        return reason;
    }

    public List<CellCoords> getValueJustifications()
    {
        return valueJustifications;
    }

    public Map<CellCoords, List<CellDataType>> getCandidateJustifications()
    {
        return candidateJustifications;
    }
}
