package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;

@FunctionalInterface
public interface SolverStrategyCell<CellType extends Cell<CellDataType>, CellDataType>
{
    ProposedChanges<CellType, CellDataType> process(CellType targetCell);
}