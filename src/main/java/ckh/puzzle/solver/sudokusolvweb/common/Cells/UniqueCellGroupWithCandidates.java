package ckh.puzzle.solver.sudokusolvweb.common.Cells;

/**
 * combination of UniqueCellGroup and CellGroupWithCandidates
 * @param <C> the type of cell in this group
 * @param <T> the type cells in this group store
 * @see UniqueCellGroup
 * @see CellGroupWithCandidates
 */
public interface UniqueCellGroupWithCandidates<C extends CellWithCandidates<T>, T> extends UniqueCellGroup<C, T>, CellGroupWithCandidates<C, T>
{
    //just a marker for now
}
