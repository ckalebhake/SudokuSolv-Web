package ckh.puzzle.solver.sudokusolvweb.common.Cells;

import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.InvalidCandidateException;

/**
 * represents a cell that has a fixed set of possible values, and tracks which ones have not yet been disproven
 * @param <T> type of value this cell can contain
 */
public interface CellWithCandidates<T> extends Cell<T>
{
    /**
     * @return an array of all candidates that this cell will accept
     * @see #getCurrentCandidates()
     */
    T[] getValidCandidates();

    /**
     * @return an array of all candidates that have not been disproven for this cell
     * @see #getValidCandidates()
     */
    T[] getCurrentCandidates();

    /**
     * provides all candidates which should still be considered possible at once
     * in other words, all candidates provided here will be marked as possibilities.  All others will be marked as disproven.
     * @param candidates the candidates
     * @throws InvalidCandidateException if a provided candidate cannot apply to this cell
     */
    void setCurrentCandidates(T[] candidates) throws InvalidCandidateException;

    /**
     * @param candidate candidate to check
     * @return true if the candidate has not yet been disproven
     * @throws InvalidCandidateException if the candidate does not apply to this cell
     */
    boolean checkCandidate(T candidate) throws InvalidCandidateException;

    /**
     * sets whether or not the given candidate has been disproven
     * @param candidate candidate to set
     * @param isPossible whether or not this candidate is still possible
     * @throws InvalidCandidateException if the candidate does not apply to this cell
     */
    void setCandidate(T candidate, boolean isPossible) throws InvalidCandidateException;
}
