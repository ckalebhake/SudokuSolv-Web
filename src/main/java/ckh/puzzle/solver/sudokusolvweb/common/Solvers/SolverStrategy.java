package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellGroup;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;

@FunctionalInterface
public interface SolverStrategy<BoardType extends PuzzleBoard<GroupType, CellType, CellDataType>, GroupType extends CellGroup<CellType, CellDataType>, CellType extends Cell<CellDataType>, CellDataType>
{
    ProposedChanges<CellType, CellDataType> process(BoardType targetBoard);
}
