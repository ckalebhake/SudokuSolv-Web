package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellGroup;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;
import ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Map;

/**
 * base class for other solvers to inherit from
 */
@Component
public abstract class AbstractSolver<BoardType extends PuzzleBoard<GroupType, CellType, CellDataType>, GroupType extends CellGroup<CellType, CellDataType>, CellType extends Cell<CellDataType>, CellDataType>
{
    Logger logger = Logger.getRootLogger();

    /**
     * Provides a map of objects that satisfy the SolverStrategy functional interface.
     * These can be methods, beans, or even lambdas.
     * The default solver implementation will attempt each strategy in this array in turn
     * and start the list over if any propose changes
     * for each entry in the map, the key is a solver strategy and the value is the name to show for that strategy in debug printouts.
     *
     * @return the map
     */
    @NotNull
    protected abstract Map<SolverStrategy<BoardType, GroupType, CellType, CellDataType>, String> getStrategies();

    /**
     * attempts to solve the board.  The board will be altered during solving, even if a solution is not found.
     *
     * @param toSolve PuzzleBoard to solve
     * @return whether or not solving was successful
     */
    public boolean solve(BoardType toSolve)
    {
        if (SudokuSolvConfig.shouldPrintOnSolve())
            logger.info("initial state:\n" + toSolve);

        while (!toSolve.isComplete())
        {
            if (!toSolve.isValid())
                throw new IllegalStateException("Something misbehaved and put the board in an invalid state!");

            ProposedChanges result = step(toSolve);

            if (result != null)
            {
                try
                {
                    result.applyChanges();

                    if (SudokuSolvConfig.shouldDebugSolvers())
                    {
                        logger.info("strategy: " + result.getExplanation());
                        logger.info("reasons: ");
                        for (Object justification : result.getJustifications())
                            logger.info("    " + justification);

                        toSolve.debugPrint();
                    }
                } catch (Exception e)
                {
                    logger.error("Failed to apply changes from " + result.getExplanation());
                    e.printStackTrace();
                }
            } else
            {
                if (SudokuSolvConfig.shouldDebugSolvers())
                    logger.info("no available SolverStrategy proposed changes to this board.");

                if (SudokuSolvConfig.shouldPrintOnSolve())
                    logger.info("unsolved board:\n" + toSolve);

                return false;
            }
        }

        if (SudokuSolvConfig.shouldPrintOnSolve())
        {
            logger.info("Solved board:\n" + toSolve);
        }

        return true;
    }

    /**
     * performs a single iteration of solving logic on the given puzzle board.
     * in other words, it attempts every strategy until one makes progress.
     *
     * @param toSolve board to step
     * @return a ProposedChanges object with the resulting changes and a brief explanation string set for the strategy that made the changes.
     */
    public ProposedChanges step(BoardType toSolve)
    {
        Map<SolverStrategy<BoardType, GroupType, CellType, CellDataType>, String> strategies = getStrategies();
        for (SolverStrategy<BoardType, GroupType, CellType, CellDataType> strategy : strategies.keySet())
        {
            ProposedChanges result = strategy.process(toSolve);
            if ((result != null) && (result.hasChanges()))
            {
                result.setExplanation(strategies.get(strategy));
                return result;
            }
        }

        return null;
    }
}
