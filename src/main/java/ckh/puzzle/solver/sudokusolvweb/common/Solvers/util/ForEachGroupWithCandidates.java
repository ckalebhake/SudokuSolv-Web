package ckh.puzzle.solver.sudokusolvweb.common.Solvers.util;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellGroup;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChanges;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChangesWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.SolverStrategy;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.SolverStrategyGroup;

public class ForEachGroupWithCandidates<BoardType extends PuzzleBoard<GroupType, CellType, CellDataType>, GroupType extends CellGroup<CellType, CellDataType>, CellType extends CellWithCandidates<CellDataType>, CellDataType>
        implements SolverStrategy<BoardType, GroupType, CellType, CellDataType>
{
    private SolverStrategyGroup<GroupType, CellType, CellDataType> subStrategy;

    public ForEachGroupWithCandidates(SolverStrategyGroup<GroupType, CellType, CellDataType> subStrategy)
    {
        this.subStrategy = subStrategy;
    }

    @Override
    public ProposedChangesWithCandidates<CellType, CellDataType> process(BoardType targetBoard)
    {
        ProposedChangesWithCandidates<CellType, CellDataType> combinedResults = new ProposedChangesWithCandidates<>();

        for(GroupType group: targetBoard.getGroups())
            combinedResults.join(subStrategy.process(group));

        return combinedResults;
    }
}
