package ckh.puzzle.solver.sudokusolvweb.common.Exceptions;

import java.util.Arrays;

/**
 * thrown when a CellWithCandidates is given a candidate that is invalid for that cell
 * for example, a DigitCell cannot contain 17
 */
public class InvalidCandidateException extends IllegalArgumentException
{
    public InvalidCandidateException(Object[] acceptableRange, Object actualValue)
    {
        super("A CellWithCandidates was given a value it does not accept:\n" + "expected: " + Arrays.toString(acceptableRange) + "\nactual: " + actualValue);
    }
}
