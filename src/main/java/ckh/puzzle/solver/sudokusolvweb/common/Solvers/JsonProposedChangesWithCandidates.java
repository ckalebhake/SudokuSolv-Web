package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellCoords;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JsonProposedChangesWithCandidates<CellType extends CellWithCandidates<CellDataType>, CellDataType> extends JsonProposedChanges<CellType, CellDataType>
{
    class JsonCandidateChange
    {
        private CellCoords cellCoords;
        private Set<CellDataType> candidatesToRemove;

        JsonCandidateChange(CellCoords cellCoords, Set<CellDataType> candidatesToRemove)
        {
            this.cellCoords = cellCoords;
            this.candidatesToRemove = candidatesToRemove;
        }

        public CellCoords getCellCoords()
        {
            return cellCoords;
        }

        public void setCellCoords(CellCoords cellCoords)
        {
            this.cellCoords = cellCoords;
        }

        public Set<CellDataType> getCandidatesToRemove()
        {
            return candidatesToRemove;
        }

        public void setCandidatesToRemove(Set<CellDataType> candidatesToRemove)
        {
            this.candidatesToRemove = candidatesToRemove;
        }
    }

    private List<JsonCandidateChange> candidateChanges;

    /**
     * creates a JsonProposedChanges object from a ProposedChanges object
     *
     * @param puzzleBoard     board these changes are for.  It is used to find coordinates of changed cells.
     * @param proposedChanges object to convert
     */
    public JsonProposedChangesWithCandidates(PuzzleBoard<?, CellType, CellDataType> puzzleBoard, ProposedChangesWithCandidates<CellType, CellDataType> proposedChanges)
    {
        super(puzzleBoard, proposedChanges);

        candidateChanges = new ArrayList<>();
        for (Map.Entry<CellType, Set<CellDataType>> entry : proposedChanges.getProposedCandidateEliminations().entrySet())
            candidateChanges.add(new JsonCandidateChange(puzzleBoard.findCell(entry.getKey()), entry.getValue()));
    }

    public List<JsonCandidateChange> getCandidateChanges()
    {
        return candidateChanges;
    }

    public void setCandidateChanges(List<JsonCandidateChange> candidateChanges)
    {
        this.candidateChanges = candidateChanges;
    }
}
