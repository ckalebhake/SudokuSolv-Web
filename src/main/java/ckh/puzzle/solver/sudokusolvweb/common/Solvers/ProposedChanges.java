package ckh.puzzle.solver.sudokusolvweb.common.Solvers;


import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.IllegalProposedChangesException;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.UnjustifiedChangeException;
import ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig;

import java.util.*;

import static org.apache.log4j.Logger.getRootLogger;

/**
 * contains the changes proposed by a solver function.
 * This class attempts to perform some error checking regarded to proposals, but cannot catch proposals that break the rules of the puzzle
 * for example, proposing two conflicting changes will cause an exception, but proposing to set the value of a cell whose value cannot be changed will not
 * of course, in the latter case the cell itself should still throw an exception when the changes are applied.
 * @param <CellType> Type of cell inside the group
 * @param <CellDataType> Data type stored by cells inside the group
 */
public class ProposedChanges<CellType extends Cell<CellDataType>, CellDataType>
{
    private Map<CellType, CellDataType> valueChanges;
    protected Set<ChangeProposalJustification<CellType, CellDataType>> justifications;
    private String explanation;

    public ProposedChanges()
    {
        valueChanges = new HashMap<>();
        justifications = new HashSet<>();
        explanation = null;
    }

    /**
     * @return an explanation of why these changes were proposed
     */
    public String getExplanation()
    {
        return explanation;
    }

    /**
     * sets the explanation of why these changes were proposed
     *
     * @param explanation the explanation
     */
    public void setExplanation(String explanation)
    {
        this.explanation = explanation;
    }

    public Map<CellType, CellDataType> getValueChanges()
    {
        return valueChanges;
    }

    /**
     * Proposes changing the value of the given cell.  If the given change was already proposed, it is ignored.
     * @param targetCell cell whose value would be updated
     * @param proposedValue proposed new value of the cell
     * @see ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig (if sudokuSolv.warn-unjustified-changes is set, this logs a warning)
     * @throws IllegalProposedChangesException if multiple conflicting changes are proposed
     */
    public void proposeValueChange(CellType targetCell, CellDataType proposedValue) throws IllegalProposedChangesException
    {
        proposeValueChange(targetCell, proposedValue, ChangeProposalJustification.becauseISaidSo());
    }

    /**
     * Proposes changing the value of the given cell.  If the given change was already proposed, it is ignored.
     * @param targetCell cell whose value would be updated
     * @param proposedValue proposed new value of the cell
     * @param justification justification object describing the proposed change
     * @throws IllegalProposedChangesException if multiple conflicting changes are proposed
     * @throws UnjustifiedChangeException if proposed change is unjustified and SudokuSolv.error-on-unjustified-changes is true.
     */
    public void proposeValueChange(CellType targetCell, CellDataType proposedValue, ChangeProposalJustification<CellType, CellDataType> justification) throws IllegalProposedChangesException
    {
        if (justification == null || justification.getReason() == ChangeProposalJustification.Reason.BECAUSE_I_SAID_SO)
        {
            if (SudokuSolvConfig.shouldWarnUnjustifiedChanges())
                getRootLogger().warn("Unjustified change proposed: set value of " + targetCell + " to " + proposedValue);

            if (SudokuSolvConfig.shouldErrorUnjustifiedChanges())
                throw new UnjustifiedChangeException("unjustified change proposed and this is configured to throw exceptions.");
        }

        //if changes are already proposed for this cell
        if (valueChanges.containsKey(targetCell))
        {
            if (valueChanges.get(targetCell) != proposedValue)
                throw new IllegalProposedChangesException("Multiple conflicting values proposed for the same cell"); //changes conflict
            else
                return; //ignore duplicate change
        }

        valueChanges.put(targetCell, proposedValue);
        justifications.add(justification);
    }

    /**
     * acts like proposeValueChange(), but does not take a justification and does not produce warnings or exceptions at their absence
     * used for joining two proposedChanges objects together
     * @param targetCell cell whose value would be updated
     * @param proposedValue proposed new value of the cell
     * @throws IllegalProposedChangesException if multiple conflicting changes are proposed
     */
    private void joinValueChange(CellType targetCell, CellDataType proposedValue) throws IllegalProposedChangesException
    {
        //if changes are already proposed for this cell
        if (valueChanges.containsKey(targetCell))
        {
            if (valueChanges.get(targetCell) != proposedValue)
                throw new IllegalProposedChangesException("Multiple conflicting values proposed for the same cell"); //changes conflict
            else
                return; //ignore duplicate change
        }

        valueChanges.put(targetCell, proposedValue);
    }

    /**
     * applies the proposed changes to the target group.
     */
    public void applyChanges()
    {
        for(Map.Entry<CellType, CellDataType> entry : valueChanges.entrySet())
        {
            entry.getKey().setValue(entry.getValue());
        }
    }

    /**
     * @return whether or not this object contains any actual proposals
     */
    public boolean hasChanges()
    {
        return valueChanges.size() > 0;
    }

    public Set<ChangeProposalJustification<CellType, CellDataType>> getJustifications()
    {
        return justifications;
    }

    /**
     * adds proposals from another ProposedChanges object to this one
     * @param toAdd proposals to add to this one.  The original object is not modified.
     */
    public void join(ProposedChanges<CellType, CellDataType> toAdd)
    {
        //skip null
        if (toAdd == null)
            return;

        //copy value changes
        for(Map.Entry<CellType, CellDataType> entry : toAdd.valueChanges.entrySet())
            joinValueChange(entry.getKey(), entry.getValue());

        //copy justifications
        justifications.addAll(toAdd.justifications);
    }
}
