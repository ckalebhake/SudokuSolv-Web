package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;

import javax.validation.constraints.Null;
import java.util.*;

/**
 * Justifies why a change was proposed
 */
public final class ChangeProposalJustification<CellType extends Cell<CellDataType>, CellDataType>
{
    public enum Reason
    {
        /**
         * a placeholder for when the proposer does not want to justify the change.
         * the config property sudokuSolv.warnUnjustifiedChanges will throw a warning when this is used
         */
        BECAUSE_I_SAID_SO,

        /**
         * this change is proposed because of the value of some cell
         */
        BECAUSE_OF_VALUE,

        /**
         * this change is proposed because of the values of multiple cells
         */
        BECAUSE_OF_VALUES,

        /**
         * this change is proposed because of the presence of some candidate
         */
        BECAUSE_OF_CANDIDATE,

        /**
         * this change is proposed because of the presence of multiple candidates
         */
        BECAUSE_OF_CANDIDATES,
    }

    private final Reason reason;
    private final List<CellType> valueJustifications;
    private final Map<CellType, List<CellDataType>> candidateJustifications;

    /**
     * creates a placeholder justification that just says "because I said so".
     * @param <DesiredCellType> type of cell this should reference
     * @param <DesiredCellDataType> type the DesiredCellType contains
     * @return a new ChangeProposalJustification object using reason BECAUSE_I_SAID_SO.
     * @see ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig (if sudokuSolv.warn-unjustified-changes is set, this logs a warning)
     * @see Reason for the different reasons a ChangeProposalJustification can have and what they mean
     */
    public static <DesiredCellType extends Cell<DesiredCellDataType>, DesiredCellDataType> //generic parameters of method
        ChangeProposalJustification<DesiredCellType, DesiredCellDataType>                  //method return type
        becauseISaidSo()                                                                   //method name/parameters
    {
        return new ChangeProposalJustification<DesiredCellType, DesiredCellDataType>(Reason.BECAUSE_I_SAID_SO, null, null);
    }

    /**
     * justifies the change as being due to the value of the specified cell
     * @param cellContainingReason the cell whose value justifies the change
     * @param <DesiredCellType> type of cell this should reference
     * @param <DesiredCellDataType> type the DesiredCellType contains
     * @return a new ChangeProposalJustification object using reason BECAUSE_OF_VALUE
     * @see Reason for the different reasons a ChangeProposalJustification can have and what they mean
     * @throws IllegalArgumentException if the argument is null
     */
    public static <DesiredCellType extends Cell<DesiredCellDataType>, DesiredCellDataType>  //generic parameters of method
        ChangeProposalJustification<DesiredCellType, DesiredCellDataType>                   //method return type
    becauseOf(DesiredCellType cellContainingReason)                                //method name/parameters
    {
        if (cellContainingReason == null)
            throw new IllegalArgumentException("cellContainingReason cannot be null!");
        if (!cellContainingReason.hasValue())
            throw new IllegalArgumentException("cellContainingReason must have a value!");

        List<DesiredCellType> cellList = new ArrayList<>(1);
        cellList.add(cellContainingReason);
        return new ChangeProposalJustification<>(Reason.BECAUSE_OF_VALUE, cellList, null);
    }

    /**
     * justifies the change as being due to the values of the specified cells
     * @param cellsContainingReasons the cells whose values justifies the change
     *                               (it is acceptable for this list to only have one item,
     *                                but if you know ahead of time there will only be one then you should
     *                                just pass that instead of a list)
     * @param <DesiredCellType> type of cell this should reference
     * @param <DesiredCellDataType> type the DesiredCellType contains
     * @return a new ChangeProposalJustification object using reason BECAUSE_OF_VALUEs
     * @see Reason for the different reasons a ChangeProposalJustification can have and what they mean
     * @throws IllegalArgumentException if the argument is null
     */
    public static <DesiredCellType extends Cell<DesiredCellDataType>, DesiredCellDataType> //generic parameters of method
        ChangeProposalJustification<DesiredCellType, DesiredCellDataType>                  //method return type
    becauseOf(List<DesiredCellType> cellsContainingReasons)                                //method name/parameters
    {
        if (cellsContainingReasons == null || cellsContainingReasons.size() == 0)
            throw new IllegalArgumentException("cellsContainingReasons cannot be null or empty!");

        for (DesiredCellType cellContainingReason : cellsContainingReasons)
            if (!cellContainingReason.hasValue())
                throw new IllegalArgumentException("all cells in the list must have a value!");

        return new ChangeProposalJustification<>(Reason.BECAUSE_OF_VALUES, cellsContainingReasons, null);
    }

    /**
     * justifies the change as being due to some specific cell having (or not having) some specific candidate
     * @param cellContainingReason the cell whose containing the candidate that justifies the change
     * @param reason the candidate whose presence justifies the change
     * @param <DesiredCellType> type of cell this should reference
     * @param <DesiredCellDataType> type the DesiredCellType contains
     * @return a new ChangeProposalJustification object using reason BECAUSE_OF_CANDIDATE
     * @see Reason for the different reasons a ChangeProposalJustification can have and what they mean
     * @throws IllegalArgumentException if the argument is null
     */
    public static <DesiredCellType extends Cell<DesiredCellDataType>, DesiredCellDataType> //generic parameters of method
        ChangeProposalJustification<DesiredCellType, DesiredCellDataType>                  //method return type
        becauseOf(DesiredCellType cellContainingReason, DesiredCellDataType reason)        //method name/parameters
    {
        if (cellContainingReason == null)
            throw new IllegalArgumentException("cellsContainingReason cannot be null!");
        if (reason == null)
            throw new IllegalArgumentException("reason cannot be null!");

        List<DesiredCellDataType> list = new ArrayList<>(1);
        list.add(reason);
        Map<DesiredCellType, List<DesiredCellDataType>> map = new HashMap<>(1);
        map.put(cellContainingReason, list);
        return new ChangeProposalJustification<>(Reason.BECAUSE_OF_CANDIDATE, null, map);
    }

    /**
     * justifies the change as being due to the presence (or absence) of multiple candidates contained in one or more cells
     * @param reasons a map of lists containing the candidates that justify the change
     *                each key is a cell that contains justifying candidates
     *                each value is a list of candidates within that cell that justify the change
     * @param <DesiredCellType> type of cell this should reference
     * @param <DesiredCellDataType> type the DesiredCellType contains
     * @return a new ChangeProposalJustification object using reason BECAUSE_OF_CANDIDATE
     * @see Reason for the different reasons a ChangeProposalJustification can have and what they mean
     * @throws IllegalArgumentException if the argument is null
     */
    public static <DesiredCellType extends Cell<DesiredCellDataType>, DesiredCellDataType> //generic parameters of method
        ChangeProposalJustification<DesiredCellType, DesiredCellDataType>                  //method return type
        becauseOf(Map<DesiredCellType, List<DesiredCellDataType>> reasons)                 //method name/parameters
    {
        if (reasons == null || reasons.size() == 0)
            throw new IllegalArgumentException("reasons cannot be null or empty!");

        for (List<DesiredCellDataType> list : reasons.values())
            if (list == null || list.size() == 0)
                throw new IllegalArgumentException("no value in reasons can be null or empty!");

        return new ChangeProposalJustification<>(Reason.BECAUSE_OF_CANDIDATES, null, reasons);
    }

    private ChangeProposalJustification(Reason reason,
                                        List<CellType> valueJustifications,
                                        Map<CellType, List<CellDataType>> candidateJustifications)
    {
        this.reason = reason;
        this.valueJustifications = valueJustifications;
        this.candidateJustifications = candidateJustifications;
    }

    public Reason getReason()
    {
        return reason;
    }

    public List<CellType> getValueJustifications()
    {
        return valueJustifications;
    }

    public Map<CellType, List<CellDataType>> getCandidateJustifications()
    {
        return candidateJustifications;
    }

    @Override
    public String toString()
    {
        return "reason: " + reason;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChangeProposalJustification<?, ?> that = (ChangeProposalJustification<?, ?>) o;

        if (reason != that.reason) return false;
        if (valueJustifications != null ? !new HashSet<>(valueJustifications).equals(new HashSet<>(that.valueJustifications)) : that.valueJustifications != null)
            return false;
        return candidateJustifications != null ? candidateJustifications.equals(that.candidateJustifications) : that.candidateJustifications == null;
    }

    @Override
    public int hashCode()
    {
        int result = reason.hashCode();
        result = 31 * result + (valueJustifications != null ? valueJustifications.hashCode() : 0);
        result = 31 * result + (candidateJustifications != null ? candidateJustifications.hashCode() : 0);
        return result;
    }
}
