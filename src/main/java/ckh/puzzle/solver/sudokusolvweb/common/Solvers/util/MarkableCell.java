package ckh.puzzle.solver.sudokusolvweb.common.Solvers.util;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;

/**
 * convenience container to pair a Cell reference with a boolean for use with strategies that want to keep track of something.
 *
 * @param <CellType> type of cell being stored
 */
public class MarkableCell<CellType extends Cell>
{
    private final CellType cellReference;
    private boolean isMarked;

    public MarkableCell(CellType cell)
    {
        cellReference = cell;
        isMarked = false;
    }

    public CellType cell()
    {
        return cellReference;
    }

    public boolean isMarked()
    {
        return isMarked;
    }

    public void setMarked(boolean isMarked)
    {
        this.isMarked = isMarked;
    }

    public void mark()
    {
        isMarked = true;
    }

    public void unmark()
    {
        isMarked = false;
    }
}
