package ckh.puzzle.solver.sudokusolvweb.common;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellCoords;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellGroup;

import java.util.List;

public interface PuzzleBoard<GroupType extends CellGroup<CellType, CellDataType>, CellType extends Cell<CellDataType>, CellDataType>
{
    /**
     * prints the board with full debug info.  This should show the complete state of the board.
     */
    void debugPrint();

    /**
     * gets all cell groups on the board
     * the meaning of this depends on the puzzle.
     * For example, on a Sudoku board each "group" is a row, column, or 3x3 box.
     * The point is a group should be a set of cells for SolverStrategy objects to work on
     * @return a list containing the groups
     */
    GroupType[] getGroups();

    /**
     * @return whether or not the board is still "valid".  (in other words, this should return false if any puzzle rules are violated)
     */
    default boolean isValid()
    {
        for (GroupType group : getGroups())
            if (!group.isValid())
                return false;

        return true;
    }

    /**
     * @return whether or not the board is complete.  (note that this does not require the solution is correct!)
     */
    default boolean isComplete()
    {
        for (GroupType group : getGroups())
            if (!group.isComplete())
                return false;

        return true;
    }

    /**
     * @return a read-only list of cells in the board.  (the cells themselves can still be modified, just not the list)
     */
    List<CellType> getCells();

    /**
     * finds the given cell within this board
     *
     * @param cell cell to find
     * @return location of the given cell on the board, or null if it is not found
     */
    CellCoords findCell(CellType cell);
}
