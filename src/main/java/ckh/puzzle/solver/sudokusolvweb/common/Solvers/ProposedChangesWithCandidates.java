package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.IllegalProposedChangesException;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.UnjustifiedChangeException;
import ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig;

import java.util.*;

import static org.apache.log4j.Logger.getRootLogger;

/**
 * contains the changes proposed by a solver function.  This version includes support for proposing candidate changes.
 * @param <CellType> Type of cell inside the group
 * @param <CellDataType> Data type stored by cells inside the group
 */
public class ProposedChangesWithCandidates<CellType extends CellWithCandidates<CellDataType>, CellDataType>
        extends ProposedChanges<CellType, CellDataType>
{
    private Map<CellType, Set<CellDataType>> proposedCandidateEliminations;

    public ProposedChangesWithCandidates()
    {
        proposedCandidateEliminations = new HashMap<>();
    }

    /**
     * proposes eliminating the specified candidate
     * @param targetCell cell to propose changes to
     * @param toEliminate candidate to propose eliminating
     * @see ckh.puzzle.solver.sudokusolvweb.config.SudokuSolvConfig (if sudokuSolv.warn-unjustified-changes is set, this logs a warning)
     * @throws IllegalProposedChangesException if an invalid candidate is proposed or if the new proposal would eliminate the last candidate on the cell
     */
    public void proposeCandidateElimination(CellType targetCell, CellDataType toEliminate)
    {
        proposeCandidateElimination(targetCell, toEliminate, ChangeProposalJustification.becauseISaidSo());
    }

    /**
     * proposes eliminating the specified candidate
     * @param targetCell cell to propose changes to
     * @param toEliminate candidate to propose eliminating
     * @param justification justification for the provided change
     * @throws IllegalProposedChangesException if an invalid candidate is proposed or if the new proposal would eliminate the last candidate on the cell
     */
    public void proposeCandidateElimination(CellType targetCell, CellDataType toEliminate, ChangeProposalJustification<CellType, CellDataType> justification)
    {
        if (justification == null || justification.getReason() == ChangeProposalJustification.Reason.BECAUSE_I_SAID_SO)
        {
            if (SudokuSolvConfig.shouldWarnUnjustifiedChanges())
                getRootLogger().warn("Unjustified change proposed: eliminate candidate " + toEliminate + " on cell " + targetCell);

            if (SudokuSolvConfig.shouldErrorUnjustifiedChanges())
                throw new UnjustifiedChangeException("unjustified change proposed and this is configured to throw exceptions.");
        }

        if (!Arrays.asList(targetCell.getValidCandidates()).contains(toEliminate))
            throw new IllegalProposedChangesException("Proposed changing a candidate the target cell cannot possibly contain");

        //make sure there is a list for this cell
        proposedCandidateEliminations.putIfAbsent(targetCell, new HashSet<>());

        //add the proposal to it
        Set<CellDataType> proposedEliminations = proposedCandidateEliminations.get(targetCell);
        proposedEliminations.add(toEliminate);
        justifications.add(justification);

        if (proposedEliminations.size() >= targetCell.getCurrentCandidates().length)
            throw new IllegalProposedChangesException("Proposed eliminating all candidates on the target cell");
    }

    /**
     * acts like proposeCandidateElimination but does not require a justification and does not produce warnings or exceptions at its absence
     * @param targetCell cell to propose changes to
     * @param toEliminate candidate to propose eliminating
     * @throws IllegalProposedChangesException if an invalid candidate is proposed or if the new proposal would eliminate the last candidate on the cell
     */
    private void joinCandidateElimination(CellType targetCell, CellDataType toEliminate)
    {
        if (!Arrays.asList(targetCell.getValidCandidates()).contains(toEliminate))
            throw new IllegalProposedChangesException("Proposed changing a candidate the target cell cannot possibly contain");

        //make sure there is a list for this cell
        proposedCandidateEliminations.putIfAbsent(targetCell, new HashSet<>());

        //add the proposal to it
        Set<CellDataType> proposedEliminations = proposedCandidateEliminations.get(targetCell);
        proposedEliminations.add(toEliminate);

        if (proposedEliminations.size() >= targetCell.getCurrentCandidates().length)
            throw new IllegalProposedChangesException("Proposed eliminating all candidates on the target cell");
    }

    /**
     * applies the proposed changes to the target group.
     */
    @Override
    public void applyChanges()
    {
        super.applyChanges();

        for (Map.Entry<CellType, Set<CellDataType>> entry : proposedCandidateEliminations.entrySet())
            for(CellDataType candidate : entry.getValue())
                entry.getKey().setCandidate(candidate, false);
    }

    /**
     * @return whether or not this object contains any actual proposals
     */
    @Override
    public boolean hasChanges()
    {
        return super.hasChanges() || (proposedCandidateEliminations.size() > 0);
    }

    /**
     * adds proposals from another ProposedChanges object to this one
     * @param toAdd proposals to add to this one.  The original object is not modified.
     */
    @Override
    public void join(ProposedChanges<CellType, CellDataType> toAdd)
    {
        //skip null
        if (toAdd == null)
            return;

        //normal behavior
        super.join(toAdd);

        //if the target is also of this class (or a subclass of this class), copy over the candidate proposals as well
        if (ProposedChangesWithCandidates.class.isInstance(toAdd))
            for (Map.Entry<CellType, Set<CellDataType>> entry : ((ProposedChangesWithCandidates<CellType, CellDataType>) toAdd).proposedCandidateEliminations.entrySet())
                for (CellDataType c : entry.getValue())
                    joinCandidateElimination(entry.getKey(), c);
    }

    /**
     * gets the proposed candidate eliminations
     *
     * @return a Map of Sets.  The key is the cell to change, the value is the candidates that are proposed to be removed
     */
    public Map<CellType, Set<CellDataType>> getProposedCandidateEliminations()
    {
        return proposedCandidateEliminations;
    }
}
