package ckh.puzzle.solver.sudokusolvweb.common.Cells;

public class CellCoords
{
    private short cx;
    private short cy;

    public CellCoords(short cx, short cy)
    {
        this.cx = cx;
        this.cy = cy;
    }

    public short getCx()
    {
        return cx;
    }

    public void setCx(short cx)
    {
        this.cx = cx;
    }

    public short getCy()
    {
        return cy;
    }

    public void setCy(short cy)
    {
        this.cy = cy;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CellCoords that = (CellCoords) o;

        if (cx != that.cx) return false;
        return cy == that.cy;
    }

    @Override
    public int hashCode()
    {
        int result = (int) cx;
        result = 31 * result + (int) cy;
        return result;
    }

    @Override
    public String toString()
    {
        return "{" + cx + ", " + cy + '}';
    }
}
