package ckh.puzzle.solver.sudokusolvweb.common.Cells;

/**
 * indicates this cell can be "locked".  Frozen cells should not permit changes to their value.
 */
public interface LockableCell<T> extends Cell<T>
{
    /**
     * @return whether or not the cell is locked
     */
    boolean isLocked();

    /**
     * locks the cell, preventing changes.
     * @see #unlock()
     */
    void lock();

    /**
     * unlocks the cell, allowing changes to be made.
     * @see #lock()
     */
    void unlock();

    /**
     * unlocks the cell and also sets its value
     * @param value value to set
     * @see #unlock()
     * @see Cell#setValue(Object)
     */
    void unlockAndSetValue(T value);
}
