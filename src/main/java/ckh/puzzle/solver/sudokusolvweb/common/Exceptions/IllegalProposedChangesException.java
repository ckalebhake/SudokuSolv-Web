package ckh.puzzle.solver.sudokusolvweb.common.Exceptions;

/**
 * thrown when illegal changes are proposed to the given cell
 */
public class IllegalProposedChangesException extends IllegalStateException
{
    public IllegalProposedChangesException(String message)
    {
        super(message);
    }
}
