package ckh.puzzle.solver.sudokusolvweb.common.Solvers.util;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellGroup;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChanges;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChangesWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.SolverStrategy;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.SolverStrategyCell;

public class ForEachCellWithCandidates<BoardType extends PuzzleBoard<GroupType, CellType, CellDataType>, GroupType extends CellGroup<CellType, CellDataType>, CellType extends CellWithCandidates<CellDataType>, CellDataType>
    implements SolverStrategy<BoardType, GroupType, CellType, CellDataType>
{
    private SolverStrategyCell<CellType, CellDataType> subStrategy;

    public ForEachCellWithCandidates(SolverStrategyCell<CellType, CellDataType> subStrategy)
    {
        this.subStrategy = subStrategy;
    }

    @Override
    public ProposedChanges<CellType, CellDataType> process(BoardType targetBoard)
    {
        ProposedChangesWithCandidates<CellType, CellDataType> result = new ProposedChangesWithCandidates<>();

        for (CellType c : targetBoard.getCells())
            result.join(subStrategy.process(c));

        return result;
    }
}
