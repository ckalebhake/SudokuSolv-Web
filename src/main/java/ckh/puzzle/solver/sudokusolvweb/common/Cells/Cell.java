package ckh.puzzle.solver.sudokusolvweb.common.Cells;

import com.sun.istack.Nullable;

/**
 * represents a cell on a puzzle board
 * @param <T> type of value the cell holds
 */
public interface Cell<T>
{
    /**
     * @return whether or not this cell has a set value
     */
    boolean hasValue();

    /**
     * @return the value of the cell, or null if it has no value
     */
    @Nullable
    T getValue();

    /**
     * sets the value of the cell
     * @param value value to set
     */
    void setValue(T value);

    /**
     * removes the value of this cell
     * @see #reset()
     */
    void clear();

    /**
     * returns this cell to its initial state
     * @see #clear()
     */
    void reset();
}
