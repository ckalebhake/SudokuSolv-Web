package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.InvalidSudokuException;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.UniqueCellGroupWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.InvalidPuzzleException;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.util.MarkableCell;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * container class for basic SolverStrategy functions that work with candidates in groups that cannot contain more than one cell of the same value
 * each of these functions should abide by one of the SolverStrategy interfaces
 * note that some solver functions are inherited from superclasses
 *
 * @see BasicCandidateStrategies
 * @see SolverStrategy
 * @see SolverStrategyGroup
 * @see SolverStrategyCell
 */
@Component
public class BasicUniqueCandidateStrategies<BoardType extends PuzzleBoard<GroupType, CellType, CellDataType>, GroupType extends UniqueCellGroupWithCandidates<CellType, CellDataType>, CellType extends CellWithCandidates<CellDataType>, CellDataType>
        extends BasicCandidateStrategies<BoardType, GroupType, CellType, CellDataType>
{
    /**
     * eliminates all candidates in the given group where another cell already has that same value
     *
     * @param targetGroup group to check
     * @return a ProposedChangesWithCandidates containing the proposed eliminations.  This object may be empty.
     */
    public ProposedChangesWithCandidates<CellType, CellDataType> removeCandidatesForExistingValues(GroupType targetGroup)
    {
        ProposedChangesWithCandidates<CellType, CellDataType> result = new ProposedChangesWithCandidates<>(); //container for result

        for (CellType cellWithKnownValue : targetGroup.getCells()) //for every known value
            if (cellWithKnownValue.hasValue())
                for (CellType cell : targetGroup.getCells()) //for every cell
                    if (!cell.hasValue()) //that is not solved
                        if (cell.checkCandidate(cellWithKnownValue.getValue())) //if the cell has not been solved yet and if the value is a possible candidate
                            result.proposeCandidateElimination(cell, cellWithKnownValue.getValue(), //propose eliminating that candidate
                                    ChangeProposalJustification.becauseOf(cellWithKnownValue));     //because of the value of this cell

        return result;
    }

    /**
     * if there are two cells in this group containing the same pair of candidates with no other values, then
     * those candidates can be removed from every other cell in the group
     * (ex: two cells contain only candidates 1 and 2.  1 and 2 can be removed from every other cell in the group)
     * the name comes from the SudokuWiki terminology: http://www.sudokuwiki.org/Naked_Candidates#NP
     *
     * @param targetGroup group to check
     * @return a ProposedChangesWithCandidates containing the proposed eliminations.  This object may be null or empty.
     * @throws InvalidSudokuException if
     */
    public ProposedChangesWithCandidates<CellType, CellDataType> nakedPairs(GroupType targetGroup)
    {
        //gather up cells that contain exactly two candidates
        ArrayList<MarkableCell<CellType>> cellsWithTwoCandidates = new ArrayList<>();

        for (CellType c : targetGroup.getCells())
            if (c.getCurrentCandidates().length == 2)
                cellsWithTwoCandidates.add(new MarkableCell<>(c));

        //if there are not enough, bail immediately
        if (cellsWithTwoCandidates.size() < 2)
            return null;

        ProposedChangesWithCandidates<CellType, CellDataType> result = new ProposedChangesWithCandidates<>();

        //for each cell in the list, look for another with a matching pair of candidates
        for (int i = 0; i < cellsWithTwoCandidates.size() - 1; i++)
        {
            MarkableCell<CellType> mci = cellsWithTwoCandidates.get(i);

            //skip cells that have already been processed
            if (mci.isMarked())
                continue;

            //see if a cell matches mci
            MarkableCell<CellType> mcj = null;
            for (int j = i + 1; j < cellsWithTwoCandidates.size(); j++)
            {
                if (mci.isMarked())
                    continue;

                if (Arrays.equals(mci.cell().getCurrentCandidates(), cellsWithTwoCandidates.get(j).cell().getCurrentCandidates()))
                {
                    //mci and this cell form a naked pair
                    if (mcj == null)
                        mcj = cellsWithTwoCandidates.get(j);
                    else
                        throw new InvalidPuzzleException("There are three (or more) cells in a group that have the same set of exactly two candidates.  This means puzzle cannot be solved, as it is illegal for groups of this type to contain duplicate values.");
                }
            }

            //we found a naked pair.  Mark both cells as processed so they arent checked again, and propose our changes
            if (mcj != null)
            {
                mci.mark();
                mcj.mark();

                //candidates in the pair
                CellDataType ca = mci.cell().getCurrentCandidates()[0];
                CellDataType cb = mci.cell().getCurrentCandidates()[1];

                //justification we will use for all of our proposed changes
                Map<CellType, List<CellDataType>> reasons = new HashMap<>();
                reasons.put(mci.cell(), Arrays.asList(ca, cb));
                reasons.put(mcj.cell(), Arrays.asList(ca, cb));
                ChangeProposalJustification<CellType, CellDataType> justification = ChangeProposalJustification.becauseOf(reasons);

                //propose changes
                for (CellType c : targetGroup.getCells())
                {
                    if ( (c == mci.cell()) || (c == mcj.cell()) )
                        continue;

                    if (c.checkCandidate(ca))
                        result.proposeCandidateElimination(c, ca, justification);

                    if (c.checkCandidate(cb))
                        result.proposeCandidateElimination(c, cb, justification);
                }
            }
        }

        return result;
    }

    /**
     * searches for a pair of required candidates that only exist in two cells in the group, and proposes eliminating all other candidates in those groups
     * the name is based on the SudokuWiki terminology.  See http://www.sudokuwiki.org/Hidden_Candidates#HP
     *
     * @param targetGroup group to check
     * @return a ProposedChangesWithCandidates containing the proposed eliminations.  This object may be null or empty.
     * @throws InvalidSudokuException if
     */
    public ProposedChangesWithCandidates<CellType, CellDataType> hiddenPairs(GroupType targetGroup)
    {
        CellDataType[] requiredCandidates = targetGroup.groupRequiredCandidates();

        //if there arent at least 2 required candidates, forming pairs will be impossible.  bail early.
        if (requiredCandidates.length < 2)
            return null;

        //look for a pair of candidates that is only present in two cells
        candidateLoop:
        for (int i = 0; i < requiredCandidates.length - 1; i++)
        {
            CellType cellA = null;
            CellType cellB = null;

            for (CellType c : targetGroup.getCells())
            {
                if (!c.hasValue() && c.checkCandidate(requiredCandidates[i]))
                {
                    //found a cell with this candidate
                    if (cellA == null)
                        cellA = c;
                    else
                    {
                        if (cellB == null)
                            cellB = c;
                        else
                            continue candidateLoop; //there are three (or more) cells with this candidate, so it cant form a hidden pair
                    }
                }
            }

            if (cellA == null || cellB == null)
                continue;

            //there are exactly two cells with this candidate.  See if a candidate we havent checked yet can be found in only those two cells
            for (int j = i + 1; j < requiredCandidates.length; j++)
            {
                short rcjCount = 0;
                for (CellType c : targetGroup.getCells())
                    if (c.checkCandidate(requiredCandidates[j]))
                        rcjCount++;

                if ( (rcjCount != 2) || (!cellA.checkCandidate(requiredCandidates[j])) || (!cellB.checkCandidate(requiredCandidates[j])) )
                    continue;

                //we found a hidden pair!

                //create a justification we will use for all changes
                Map<CellType, List<CellDataType>> reasons = new HashMap<>();
                reasons.put(cellA, Arrays.asList(requiredCandidates[i], requiredCandidates[j]));
                reasons.put(cellB, Arrays.asList(requiredCandidates[i], requiredCandidates[j]));
                ChangeProposalJustification<CellType, CellDataType> justification = ChangeProposalJustification.becauseOf(reasons);

                ProposedChangesWithCandidates<CellType, CellDataType> result = new ProposedChangesWithCandidates<>();

                for(CellDataType candidate : cellA.getCurrentCandidates())
                    if ( (candidate != requiredCandidates[i]) && (candidate != requiredCandidates[j]) )
                        result.proposeCandidateElimination(cellA, candidate, justification);

                for(CellDataType candidate : cellB.getCurrentCandidates())
                    if ( (candidate != requiredCandidates[i]) && (candidate != requiredCandidates[j]) )
                        result.proposeCandidateElimination(cellB, candidate, justification);

                return result;
            }
        }

        //loop completed without finding anything
        return null;
    }
}
