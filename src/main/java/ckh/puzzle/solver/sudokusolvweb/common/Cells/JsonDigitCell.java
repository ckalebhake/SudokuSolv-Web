package ckh.puzzle.solver.sudokusolvweb.common.Cells;

import java.util.Arrays;

/**
 * A DigitCell as represented in json
 * @see DigitCell
 */
public class JsonDigitCell
{
    private byte value;
    private boolean[] candidates;
    private boolean fixed;

    public byte getValue()
    {
        return value;
    }

    public void setValue(byte value)
    {
        if (value < 0 || value > 9)
            throw new IllegalArgumentException("can only have values 0 - 9");

        this.value = value;
    }

    public boolean[] getCandidates()
    {
        return candidates;
    }

    public void setCandidates(boolean[] candidates)
    {
        if (candidates.length != 9)
            throw new IllegalArgumentException("must have exactly 9 arguments");

        this.candidates = candidates;
    }

    public boolean isFixed()
    {
        return fixed;
    }

    public void setFixed(boolean fixed)
    {
        this.fixed = fixed;
    }

    @Override
    public String toString()
    {
        return '{' +
                "value=" + value +
                ", candidates=" + Arrays.toString(candidates) +
                ", fixed=" + fixed +
                '}';
    }

    /**
     * creates a blank cell
     */
    public JsonDigitCell() { }

    /**
     * creates a JsonDigitCell to represent the given DigitCell
     */
    public JsonDigitCell(DigitCell digitCell)
    {
        value = (digitCell.hasValue()) ? digitCell.getValue() : 0;

        candidates = new boolean[] {false, false, false, false, false, false, false, false, false};
        for (Byte candidate : digitCell.getCurrentCandidates())
            candidates[candidate - 1] = true;

        fixed = digitCell.isLocked();
    }
}
