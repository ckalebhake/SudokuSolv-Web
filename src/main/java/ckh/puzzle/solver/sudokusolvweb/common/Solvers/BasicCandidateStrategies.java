package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellGroupWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;
import org.springframework.stereotype.Component;

/**
 * container class for basic SolverStrategy functions that work with candidates
 * each of these functions should abide by one of the SolverStrategy
 *
 * @see SolverStrategy
 * @see SolverStrategyGroup
 * @see SolverStrategyCell
 */
@Component
public class BasicCandidateStrategies<BoardType extends PuzzleBoard<GroupType, CellType, CellDataType>, GroupType extends CellGroupWithCandidates<CellType, CellDataType>, CellType extends CellWithCandidates<CellDataType>, CellDataType>
{
    /**
     * sets the value of a cell if there is only one valid candidate for said cell
     *
     * @param targetCell cell to check
     * @return the ProposedChanges, or null if there are none
     */
    public ProposedChanges<CellType, CellDataType> setValueIfOnlyOneOption(CellType targetCell)
    {
        //if the cell is not already solved
        if (!targetCell.hasValue())
        {
            //but it only has one candidate
            CellDataType[] candidates = targetCell.getCurrentCandidates();
            if (candidates.length == 1)
            {
                //then set the value
                ProposedChanges<CellType, CellDataType> result = new ProposedChanges<>();
                result.proposeValueChange(targetCell, candidates[0], ChangeProposalJustification.becauseOf(targetCell, candidates[0]));
                return result;
            }
        }

        return null; //otherwise no changes
    }


    /**
     * Finds required candidates that can only go in one place within the group, and puts them there.
     * the name comes from terminology used by SudokuWiki.org: http://www.sudokuwiki.org/Getting_Started
     *
     * @param targetGroup group to check
     * @return a ProposedChangesWithCandidates containing the proposed changes.  This object may be empty.
     */
    public ProposedChangesWithCandidates<CellType, CellDataType> hiddenSingles(GroupType targetGroup)
    {
        CellDataType[] candidates = targetGroup.groupRequiredCandidates();

        ProposedChangesWithCandidates<CellType, CellDataType> result = new ProposedChangesWithCandidates<>();

        //for each candidate
        candidateLoop:
        for (CellDataType candidate : candidates)
        {
            //search for cells that have it
            CellType onlyCellWithCandidate = null;
            for (CellType cell : targetGroup.getCells())
            {
                if (!cell.hasValue())
                {
                    if (cell.checkCandidate(candidate))
                    {
                        //found a cell that has this candidate.
                        if (onlyCellWithCandidate != null)
                            continue candidateLoop; //found more than one cell with this candidate.  Move on to the next one
                        else
                            onlyCellWithCandidate = cell;
                    }
                }
            }

            if (onlyCellWithCandidate != null)
            {
                //there is one and only one cell in this group that can be this candidate.  Propose setting the value.
                result.proposeValueChange(onlyCellWithCandidate, candidate,
                        ChangeProposalJustification.becauseOf(onlyCellWithCandidate, candidate));
            }
        }

        return result;
    }
}
