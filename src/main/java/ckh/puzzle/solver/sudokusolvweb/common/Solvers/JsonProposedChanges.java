package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellCoords;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Json representation of a ProposedChanges object.
 *
 * @param <CellType>     type of cell these changes are for
 * @param <CellDataType> type contained within CellType
 */
public class JsonProposedChanges<CellType extends Cell<CellDataType>, CellDataType>
{
    class JsonValueChange
    {
        private CellCoords cellCoords;
        private CellDataType newValue;

        JsonValueChange(CellCoords cellCoords, CellDataType newValue)
        {
            this.cellCoords = cellCoords;
            this.newValue = newValue;
        }

        public CellCoords getCellCoords()
        {
            return cellCoords;
        }

        public void setCellCoords(CellCoords cellCoords)
        {
            this.cellCoords = cellCoords;
        }

        public CellDataType getNewValue()
        {
            return newValue;
        }

        public void setNewValue(CellDataType newValue)
        {
            this.newValue = newValue;
        }
    }

    private String explanation;
    private List<JsonValueChange> valueChanges;
    private Set<JsonChangeProposalJustification<CellType, CellDataType>> justifications;

    /**
     * creates a JsonProposedChanges object from a ProposedChanges object
     *
     * @param puzzleBoard     board these changes are for.  It is used to find coordinates of changed cells.
     * @param proposedChanges object to convert
     */
    public JsonProposedChanges(PuzzleBoard<?, CellType, CellDataType> puzzleBoard, ProposedChanges<CellType, CellDataType> proposedChanges)
    {
        explanation = proposedChanges.getExplanation();

        valueChanges = new ArrayList<>();

        for (Map.Entry<CellType, CellDataType> valueChange : proposedChanges.getValueChanges().entrySet())
            valueChanges.add(new JsonValueChange(puzzleBoard.findCell(valueChange.getKey()), valueChange.getValue()));

        this.justifications = proposedChanges
                .getJustifications()
                .stream()
                .map(justification -> new JsonChangeProposalJustification<>(puzzleBoard, justification))
                .collect(Collectors.toSet());
    }

    public String getExplanation()
    {
        return explanation;
    }

    public void setExplanation(String explanation)
    {
        this.explanation = explanation;
    }

    public List<JsonValueChange> getValueChanges()
    {
        return valueChanges;
    }

    public void setValueChanges(List<JsonValueChange> valueChanges)
    {
        this.valueChanges = valueChanges;
    }

    public Set<JsonChangeProposalJustification<CellType, CellDataType>> getJustifications()
    {
        return justifications;
    }

    public void setJustifications(Set<JsonChangeProposalJustification<CellType, CellDataType>> justifications)
    {
        this.justifications = justifications;
    }
}
