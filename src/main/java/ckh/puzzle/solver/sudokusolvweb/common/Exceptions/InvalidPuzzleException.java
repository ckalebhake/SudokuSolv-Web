package ckh.puzzle.solver.sudokusolvweb.common.Exceptions;

/**
 * thrown when a puzzle is found to be invalid (such as having no solution, or violating its own rules)
 */
public class InvalidPuzzleException extends RuntimeException
{
    public InvalidPuzzleException()
    {
    }

    public InvalidPuzzleException(String message)
    {
        super(message);
    }

    public InvalidPuzzleException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InvalidPuzzleException(Throwable cause)
    {
        super(cause);
    }

    public InvalidPuzzleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
