package ckh.puzzle.solver.sudokusolvweb.common.Cells;

/**
 * represents a group of cells with candidates
 * @param <C> the type of cell in this group
 * @param <T> the type cells in this group store
 */
public interface CellGroupWithCandidates<C extends CellWithCandidates<T>, T> extends CellGroup<C, T>
{
    /**
     * finds all values that are a candidate in at least one cell within the group.
     * @see #groupRequiredCandidates()
     * @return an array with all unique current candidates across all cells in the group
     */
    T[] groupPossibleCandidates();

    /**
     * finds all values that must be placed somewhere within the group.
     * Note that this does NOT include candidates that MIGHT be placed within the group
     * it also does not include values that have ALREADY been placed in the group, unless that value must appear at least one more time than it already does.
     * @return an array with all unique candidates that must go somewhere in this group
     */
    T[] groupRequiredCandidates();
}
