package ckh.puzzle.solver.sudokusolvweb.common.Solvers;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.Cell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellGroup;

@FunctionalInterface
public interface SolverStrategyGroup<GroupType extends CellGroup<CellType, CellDataType>, CellType extends Cell<CellDataType>, CellDataType>
{
    ProposedChanges<CellType, CellDataType> process(GroupType targetGroup);
}