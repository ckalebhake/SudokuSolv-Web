package ckh.puzzle.solver.sudokusolvweb.common.Cells;

import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.InvalidCandidateException;

import java.util.ArrayList;

/**
 * represents a cell that could have any value from 1 - 9.
 * the cell can be "locked", which simply means that its value should not be changed.
 * usually this means it was provided as part of the initial state of the board.
 * @see Cell
 * @see CellWithCandidates
 * @see LockableCell
 */
public class DigitCell implements LockableCell<Byte>, CellWithCandidates<Byte>
{
    //values accepted by DigitCell
    private static final Byte[] validCandidates = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    private Byte        initialValue;
    private Byte        value;
    private boolean[]   initialCandidates;
    private boolean[]   currentCandidates;
    private boolean     isLocked;

    public DigitCell()
    {
        initialValue = null;
        value = null;
        currentCandidates = new boolean[] {true, true, true, true, true, true, true, true, true};
        initialCandidates = new boolean[] {true, true, true, true, true, true, true, true, true};
        isLocked = false;
    }

    public DigitCell(Byte fixedValue) throws InvalidCandidateException
    {
        this();

        if (fixedValue != null)
        {
            throwExceptionIfInvalid(fixedValue);
            initialCandidates = new boolean[] {false, false, false, false, false, false, false, false, false};
            initialCandidates[fixedValue - 1] = true;
            initialValue = fixedValue;
            value = fixedValue;
            currentCandidates = initialCandidates.clone();
            isLocked = true;
        }
    }

    /**
     * constructs a new DigitCell to match the data in the jsonDigitCell
     * @param jsonDigitCell the json representation
     */
    public DigitCell(JsonDigitCell jsonDigitCell)
    {
        if (jsonDigitCell.isFixed())
        {
            initialValue = jsonDigitCell.getValue();
            initialCandidates = new boolean[] {false, false, false, false, false, false, false, false, false};
            initialCandidates[jsonDigitCell.getValue() - 1] = true;
        }
        else
        {
            initialValue = null;
            initialCandidates = new boolean[] {true, true, true, true, true, true, true, true, true};
        }

        value = (jsonDigitCell.getValue() == 0) ? null : jsonDigitCell.getValue();
        currentCandidates = jsonDigitCell.getCandidates();
        isLocked = jsonDigitCell.isFixed();
    }

    /**
     * provides all candidates which should still be considered possible at once
     * in other words, all candidates provided here will be marked as possibilities.  All others will be marked as disproved.
     *
     * @param newCandidates the candidates
     * @throws InvalidCandidateException if a provided candidate cannot apply to this cell
     * @throws IllegalArgumentException if the provided array is null.
     */
    @Override
    public void setCurrentCandidates(Byte[] newCandidates) throws InvalidCandidateException
    {
        if (isLocked)
            throw new IllegalStateException("Cannot modify cell: it is locked!");

        if (newCandidates == null)
            throw new IllegalArgumentException("Cannot set candidates to null array: pass an empty array instead.");

        //operate on a temporary array so we don't lose our current data if something goes wrong
        boolean[] tempCandidates = {false, false, false, false, false, false, false, false, false};

        //set the values
        for(Byte c : newCandidates)
        {
            throwExceptionIfInvalid(c);

            tempCandidates[c - 1] = true;
        }

        //nothing went wrong: replace the current set with the new one
        currentCandidates = tempCandidates;
    }

    /**
     * @return an array of all candidates that this cell will accept
     * @see #getCurrentCandidates()
     */
    @Override
    public Byte[] getValidCandidates()
    {
        return validCandidates;
    }

    /**
     * @return an array of all candidates that have not been disproved for this cell
     * @see #getValidCandidates()
     */
    @Override
    public Byte[] getCurrentCandidates()
    {
        ArrayList<Byte> result = new ArrayList<>(9);

        for (byte i = 1; i <= 9; i++)
            if (currentCandidates[i - 1])
                result.add(i);

        return result.toArray(new Byte[result.size()]);
    }

    /**
     * @param candidate candidate to check
     * @return true if the candidate has not yet been disproved
     * @throws InvalidCandidateException if the candidate does not apply to this cell
     */
    @Override
    public boolean checkCandidate(Byte candidate) throws InvalidCandidateException
    {
        throwExceptionIfInvalid(candidate);

        return currentCandidates[candidate - 1];
    }

    /**
     * sets whether or not the given candidate has been disproved
     *
     * @param candidate  candidate to set
     * @param isPossible whether or not this candidate is still possible
     * @throws InvalidCandidateException if the candidate does not apply to this cell
     * @throws IllegalStateException if the cell is locked
     */
    @Override
    public void setCandidate(Byte candidate, boolean isPossible) throws InvalidCandidateException
    {
        if (isLocked)
            throw new IllegalStateException("Cannot set candidate: cell is locked");

        throwExceptionIfInvalid(candidate);
        currentCandidates[candidate - 1] = isPossible;
    }

    /**
     * @return whether or not the cell is locked
     */
    @Override
    public boolean isLocked()
    {
        return isLocked;
    }

    /**
     * locks the cell, preventing changes.
     *
     * @see #unlock()
     */
    @Override
    public void lock()
    {
        isLocked = true;
    }

    /**
     * unlocks the cell, allowing changes to be made.
     *
     * @see #lock()
     */
    @Override
    public void unlock()
    {
        isLocked = false;
    }

    /**
     * unlocks the cell and also sets its value
     *
     * @param value value to set
     * @see #unlock()
     * @see Cell#setValue(Object)
     */
    @Override
    public void unlockAndSetValue(Byte value)
    {
        unlock();
        setValue(value);
    }

    /**
     * @return whether or not this cell has a set value
     */
    @Override
    public boolean hasValue()
    {
        return value != null;
    }

    /**
     * @return the value of the cell, or null if it has no value
     */
    @Override
    public Byte getValue()
    {
        return value;
    }

    /**
     * sets the value of the cell
     *
     * @param value value to set
     * @throws IllegalStateException if the cell is locked
     */
    @Override
    public void setValue(Byte value)
    {
        if (isLocked)
            throw new IllegalStateException("cannot set value: cell is locked");

        throwExceptionIfInvalid(value);
        currentCandidates[value - 1] = true;
        this.value = value;
    }

    /**
     * removes the value of this cell
     *
     * @see #reset()
     */
    @Override
    public void clear()
    {
        value = null;
    }

    /**
     * returns this cell to its initial state
     *
     * @see #clear()
     */
    @Override
    public void reset()
    {
        value = initialValue;
        currentCandidates = initialCandidates.clone();
        isLocked = (initialValue != null);
    }

    /**
     * throws an InvalidCandidateException if the given value is null or outside of the accepted range
     * @param toCheck value to check
     * @throws InvalidCandidateException if the given value is null or outside of the accepted range
     */
    private void throwExceptionIfInvalid(Byte toCheck) throws InvalidCandidateException
    {
        if (toCheck == null || toCheck < 1 || toCheck > 9)
            throw new InvalidCandidateException(validCandidates, toCheck);
    }

    @Override
    public String toString()
    {
        return (value == null) ? " " : value.toString();
    }

    
}
