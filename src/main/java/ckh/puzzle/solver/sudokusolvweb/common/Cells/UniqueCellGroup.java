package ckh.puzzle.solver.sudokusolvweb.common.Cells;

/**
 * represents a cell group that is not allowed to have the same value twice
 * @param <C> the type of cell in this group
 * @param <T> the type cells in this group store
 */
public interface UniqueCellGroup<C extends Cell<T>, T> extends  CellGroup<C, T>
{
    //just a marker for now
}
