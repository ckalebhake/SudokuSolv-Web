package ckh.puzzle.solver.sudokusolvweb.common.Cells;

/**
 * a group of cells
 * @param <C> the type of cell in this group
 * @param <T> the type cells in this group store
 */
public interface CellGroup<C extends Cell<T>, T>
{
    /**
     * @return number of cells in this group
     */
    int groupLength();

    /**
     * @return an array containing all cells in the group
     */
    C[] getCells();

    /**
     * @return an array containing the values of all cells in the group that have values set
     */
    T[] getValues();

    /**
     * @return true if the group does not break any rules of the puzzle.  Note that it does NOT have to be complete!
     */
    boolean isValid();

    /**
     * @return true if all cells in this group have a set value
     */
    boolean isComplete();
}
