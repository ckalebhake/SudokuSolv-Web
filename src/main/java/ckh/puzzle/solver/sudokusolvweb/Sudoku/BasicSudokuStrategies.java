package ckh.puzzle.solver.sudokusolvweb.Sudoku;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuBoard;
import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuGroup;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.*;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * container class for basic SolverStrategy functions that are specific to Sudoku.
 * each of these functions should abide by one of the SolverStrategy interfaces
 * note that some solver functions are inherited from superclasses
 *
 * @see BasicCandidateStrategies
 * @see BasicUniqueCandidateStrategies
 * @see SolverStrategy
 * @see SolverStrategyGroup
 * @see SolverStrategyCell
 */
@Component
public class BasicSudokuStrategies extends BasicUniqueCandidateStrategies<SudokuBoard, SudokuGroup, DigitCell, Byte>
{
    /**
     * this strategy is a consequence of how groups on a sudoku board intersect with each other.
     * if every instance of some candidate within some 3x3 box on the board is within the same row or column
     * then that candidate can be removed from other cells in that row or column that lie outside the box
     * as is so often the case, the name is based on terminology at the SudokuWiki.
     * see http://www.sudokuwiki.org/Intersection_Removal#IR
     *
     * @see #boxLineReduction(SudokuBoard)
     * @param targetBoard the SudokuBoard to examine
     * @return ProposedChangesWithCandidates containing the proposed changes.  This may be empty.
     */
    public ProposedChangesWithCandidates<DigitCell, Byte> pointingPairsTriples(SudokuBoard targetBoard)
    {
        ProposedChangesWithCandidates<DigitCell, Byte> result = new ProposedChangesWithCandidates<>();

        for (byte boxX = 0; boxX < 3; boxX++)
        {
            for (byte boxY = 0; boxY < 3; boxY++)
            {
                //this is the box currently being tested
                SudokuGroup box = targetBoard.getBoxes()[(boxX * 3) + boxY];

                //check if any candidate only appears on one row or column of this box
                for (byte candidate : box.groupRequiredCandidates())
                {
                    Byte foundInSubRow = null;
                    boolean multipleSubRows = false;
                    Byte foundInSubCol = null;
                    boolean multipleSubCols = false;
                    for (byte bxr = 0; bxr < 3; bxr++)
                    {
                        for (byte bxy = 0; bxy < 3; bxy++)
                        {
                            DigitCell curCell = box.getCells()[(bxr * 3) + bxy];
                            if (curCell.checkCandidate(candidate))
                            {
                                if (foundInSubRow == null)
                                    foundInSubRow = bxr;
                                else if (foundInSubRow != bxr)
                                    multipleSubRows = true;

                                if (foundInSubCol == null)
                                    foundInSubCol = bxy;
                                else if (foundInSubCol != bxy)
                                    multipleSubCols = true;
                            }
                        }
                    }

                    //justification that will be used for any proposals made
                    HashMap<DigitCell, List<Byte>> reasons = new HashMap<>();
                    for (DigitCell cell : box.getCells()) //for every cell in this box
                        if (cell.checkCandidate(candidate)) //that has candidate as a candidate
                            reasons.put(cell, Collections.singletonList(candidate)); //list that candidate as a reason
                    ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(reasons);

                    //make proposals
                    if ( (foundInSubRow != null) && (!multipleSubRows) ) //if candidate appeared in only one subrow of this box
                        for (DigitCell cell : targetBoard.getRows()[(boxX * 3) + foundInSubRow].getCells()) //for every cell in that row
                            if (!Arrays.asList(box.getCells()).contains(cell)) //that is not in the box
                                if (cell.checkCandidate(candidate)) //and has the candidate
                                    result.proposeCandidateElimination(cell, candidate, justification); //propose removing the candidate


                    if ( (foundInSubCol != null) && (!multipleSubCols) ) //if candidate appeared in only one subrow of this box
                        for (DigitCell cell : targetBoard.getColumns()[(boxY * 3) + foundInSubCol].getCells()) //for every cell in that row
                            if (!Arrays.asList(box.getCells()).contains(cell)) //that is not in the box
                                if (cell.checkCandidate(candidate)) //and has the candidate
                                    result.proposeCandidateElimination(cell, candidate, justification); //propose removing the candidate
                }
            }
        }

        return result;
    }

    /**
     * this strategy is a consequence of how groups on a sudoku board intersect with each other.
     * if every instance of some candidate within some row or column on the board is within the same 3x3 box
     * then that candidate can be removed from other cells in that box that lie outside the row or column
     * as is so often the case, the name is based on terminology at the SudokuWiki.
     * see http://www.sudokuwiki.org/Intersection_Removal#LBR
     *
     * @see #pointingPairsTriples(SudokuBoard)
     * @param targetBoard the SudokuBoard to examine
     * @return ProposedChangesWithCandidates containing the proposed changes.  This may be empty.
     */
    public ProposedChangesWithCandidates<DigitCell, Byte> boxLineReduction(SudokuBoard targetBoard)
    {
        ProposedChangesWithCandidates<DigitCell, Byte> result = new ProposedChangesWithCandidates<>();

        //this searches diagonally (row 0, column 0, row 1, column 1...)
        for (byte i = 0; i < 9; i++)
        {
            SudokuGroup row = targetBoard.getRows()[i];
            SudokuGroup col = targetBoard.getColumns()[i];
            Byte[] rowRequiredCandidates = row.groupRequiredCandidates();
            Byte[] colRequiredCandidates = col.groupRequiredCandidates();

            //row i
            for (Byte candidate : rowRequiredCandidates)
            {
                Byte boxCol = boxLineReductionFindIntersectedBox(row, candidate);

                if (boxCol == null)
                    continue;

                //this candidate only exists within one box along this row.

                byte boxRow = (byte) (i / 3); //intentional integral division!  this is which row of boxes we are intersecting
                SudokuGroup intersectedBox = targetBoard.getBoxes()[(boxRow * 3) + boxCol];

                //justification that will be used for any proposals made
                HashMap<DigitCell, List<Byte>> reasons = new HashMap<>();
                for (DigitCell cell : row.getCells())
                    if (cell.checkCandidate(candidate))
                        reasons.put(cell, Collections.singletonList(candidate));
                ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(reasons);

                for (DigitCell cell : intersectedBox.getCells())
                    if (!Arrays.asList(row.getCells()).contains(cell))
                        if (cell.checkCandidate(candidate))
                            result.proposeCandidateElimination(cell, candidate, justification);
            }

            //column i
            for (Byte candidate : colRequiredCandidates)
            {
                Byte boxRow = boxLineReductionFindIntersectedBox(col, candidate);

                if (boxRow == null)
                    continue;

                //this candidate only exists within one box along this row.

                byte boxCol = (byte) (i / 3); //intentional integral division!  this is which row of boxes we are intersecting
                SudokuGroup intersectedBox = targetBoard.getBoxes()[(boxRow * 3) + boxCol];

                //justification that will be used for any proposals made
                HashMap<DigitCell, List<Byte>> reasons = new HashMap<>();
                for (DigitCell cell : col.getCells())
                    if (cell.checkCandidate(candidate))
                        reasons.put(cell, Collections.singletonList(candidate));
                ChangeProposalJustification<DigitCell, Byte> justification = ChangeProposalJustification.becauseOf(reasons);

                for (DigitCell cell : intersectedBox.getCells())
                    if (!Arrays.asList(col.getCells()).contains(cell))
                        if (cell.checkCandidate(candidate))
                            result.proposeCandidateElimination(cell, candidate, justification);
            }
        }

        return result;
    }

    /**
     * checks the row or column to see if the given candidate appears in only one box intersecting that row or column.
     * this is a helper for boxLineReduction()
     *
     * @param rowOrColumn the row or column to check
     * @param candidate the candidate to look for
     * @return null if the candidate does not appear or intersects multiple boxes
     *         0 if it intersects the leftmost box (for a row) or topmost box (for a column)
     *         1 if it intersects the center box
     *         2 if it intersects the rightmost box (for a row) or bottommost box (for a column)
     */
    private Byte boxLineReductionFindIntersectedBox(SudokuGroup rowOrColumn, Byte candidate)
    {
        //check if this candidate is only within one box intersecting this group
        Byte intersectedBox = null; //null: not intersecting any box.  0-2: intersecting the first/second/third box along this group

        for (byte i = 0; i < 3; i++)
            if (rowOrColumn.getCells()[i].checkCandidate(candidate))
                intersectedBox = 0;

        for (byte i = 3; i < 6; i++)
        {
            if (rowOrColumn.getCells()[i].checkCandidate(candidate))
            {
                if (intersectedBox == null)
                    intersectedBox = 1;
                else if (intersectedBox != 1)
                    return null;
            }
        }

        for (byte i = 6; i < 9; i++)
        {
            if (rowOrColumn.getCells()[i].checkCandidate(candidate))
            {
                if (intersectedBox == null)
                    intersectedBox = 2;
                else if (intersectedBox != 2)
                    return null;
            }
        }

        return intersectedBox;
    }
}
