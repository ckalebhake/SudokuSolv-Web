package ckh.puzzle.solver.sudokusolvweb.Sudoku.model;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.JsonProposedChangesWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChangesWithCandidates;

/**
 * used by the SudokuController as the response to a step call.
 * this is just a JsonProposedChangesWithCandidates that also contains the full state of the board
 * after the changes are applied
 */
public class JsonSudokuStepResult extends JsonProposedChangesWithCandidates<DigitCell, Byte>
{
    private JsonSudoku sudokuAfterChanges;

    /**
     * creates a JsonSudokuStepResult object from a SudokuBoard and a ProposedChangesWithCandidates object
     *
     * @param sudokuBoard     board these changes are for.  It is used to find coordinates of changed cells.
     * @param proposedChanges object to convert
     */
    public JsonSudokuStepResult(SudokuBoard sudokuBoard, ProposedChangesWithCandidates<DigitCell, Byte> proposedChanges)
    {
        super(sudokuBoard, proposedChanges);

        proposedChanges.applyChanges();

        sudokuAfterChanges = new JsonSudoku(sudokuBoard);
    }

    public JsonSudoku getSudokuAfterChanges()
    {
        return sudokuAfterChanges;
    }

    public void setSudokuAfterChanges(JsonSudoku boardAfterChanges)
    {
        this.sudokuAfterChanges = boardAfterChanges;
    }
}
