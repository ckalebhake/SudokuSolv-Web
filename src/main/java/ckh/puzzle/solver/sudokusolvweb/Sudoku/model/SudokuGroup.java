package ckh.puzzle.solver.sudokusolvweb.Sudoku.model;

import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.UniqueCellGroupWithCandidates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * represents a column, row, or 3x3 box on a sudoku board.
 */
public class SudokuGroup implements UniqueCellGroupWithCandidates<DigitCell, Byte>
{
    private DigitCell[] contents;

    /**
     * constructs the group from an array of existing DigitCells.
     *
     * @param contents existinc cells that should form the group.
     */
    public SudokuGroup(DigitCell[] contents)
    {
        if ((contents == null) || (contents.length != 9))
            throw new IllegalArgumentException("A SudokuGroup must have 9 cells.  Was given " + ((contents == null) ? "null array" : contents.length));

        this.contents = contents;
    }

    /**
     * finds all values that are a candidate in at least one cell within the group.
     *
     * @return an array with all unique current candidates across all cells in the group
     * @see #groupRequiredCandidates()
     */
    @Override
    public Byte[] groupPossibleCandidates()
    {
        Set<Byte> foundCandidates = new CopyOnWriteArraySet<>();

        for (int i = 0; i < 9; i++)
            if (!contents[i].hasValue())
                foundCandidates.addAll(Arrays.asList(contents[i].getCurrentCandidates()));

        return foundCandidates.toArray(new Byte[foundCandidates.size()]);
    }

    /**
     * finds all values that must be placed somewhere in this group
     * (for a SudokuGroup, this is identical to groupPossibleCandidates())
     *
     * @return an array with all unique candidates that must be placed somewhere in this group
     * @see #groupPossibleCandidates()
     */
    @Override
    public Byte[] groupRequiredCandidates()
    {
        return groupPossibleCandidates();
    }

    /**
     * @return number of cells in this group
     */
    @Override
    public int groupLength()
    {
        return 9;
    }

    /**
     * @return an array containing all cells in the group
     */
    @Override
    public DigitCell[] getCells()
    {
        return contents;
    }

    /**
     * @return an array containing the values of all cells in the group that have values set
     */
    @Override
    public Byte[] getValues()
    {
        ArrayList<Byte> values = new ArrayList<>(9);

        for (int i = 0; i < 9; i++)
            if (contents[i].hasValue())
                values.add(contents[i].getValue());

        return values.toArray(new Byte[values.size()]);
    }

    /**
     * @return true if the group does not break any rules of the puzzle.  Note that it does NOT have to be complete!
     */
    @Override
    public boolean isValid()
    {
        Set<Byte> foundValues = new CopyOnWriteArraySet<>();

        for (int i = 0; i < 9; i++)
        {
            if (contents[i].hasValue())
            {
                Byte v = contents[i].getValue();

                if (foundValues.contains(v))
                    return false;
                else
                    foundValues.add(v);
            }
        }

        return true;
    }

    /**
     * @return true if all cells in this group have a set value
     */
    @Override
    public boolean isComplete()
    {
        for (DigitCell c : contents)
            if (!c.hasValue())
                return false;

        return true;
    }

    @Override
    public String toString()
    {
        return "SudokuGroup{" +
                "contents=" + Arrays.toString(contents) +
                '}';
    }
}
