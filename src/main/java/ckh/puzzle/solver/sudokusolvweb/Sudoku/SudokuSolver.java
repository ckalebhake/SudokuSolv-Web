package ckh.puzzle.solver.sudokusolvweb.Sudoku;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuBoard;
import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuGroup;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.AbstractSolver;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChangesWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.SolverStrategy;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.util.ForEachCellWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.util.ForEachGroupWithCandidates;
import org.apache.camel.Handler;
import org.apache.camel.language.XPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * solves sudoku puzzles.  Most of the behavior of this class is inherited.
 *
 * @see AbstractSolver
 */
@Component("sudokuSolver")
public class SudokuSolver extends AbstractSolver<SudokuBoard, SudokuGroup, DigitCell, Byte>
{
    /**
     * instance of the solver object that contains solver functions
     */
    private final BasicSudokuStrategies sudokuStrategies;

    /**
     * Constructor for this solver.  It is meant to be invoked by spring, so it is private
     *
     * @param sudokuStrategies an object providing basic solver strategies for use by this solver.
     */
    @Autowired
    private SudokuSolver(BasicSudokuStrategies sudokuStrategies)
    {
        this.sudokuStrategies = sudokuStrategies;
    }

    /**
     * Provides a map of objects that satisfy the SolverStrategy functional interface.
     * These can be methods, beans, or even lambdas.
     * The default solver implementation will attempt each strategy in this array in turn
     * and start the list over if any propose changes
     * for each entry in the map, the key is a solver strategy and the value is the name to show for that strategy in debug printouts.
     *
     * @return the map
     */
    @Override
    protected Map<SolverStrategy<SudokuBoard, SudokuGroup, DigitCell, Byte>, String> getStrategies()
    {
        Map<SolverStrategy<SudokuBoard, SudokuGroup, DigitCell, Byte>, String> result = new LinkedHashMap<>();

        result.put(new ForEachCellWithCandidates<>(sudokuStrategies::setValueIfOnlyOneOption), "solve cells with only one candidate");
        result.put(new ForEachGroupWithCandidates<>(sudokuStrategies::removeCandidatesForExistingValues), "remove candidates conflicting with solved cells");
        result.put(new ForEachGroupWithCandidates<>(sudokuStrategies::hiddenSingles), "hidden singles (only one cell in group can contain value)");
        result.put(new ForEachGroupWithCandidates<>(sudokuStrategies::nakedPairs), "naked pairs (two cells with only the same two candidates in the group means those can be removed from others in the group)");
        result.put(new ForEachGroupWithCandidates<>(sudokuStrategies::hiddenPairs), "hidden pairs (two required candidates can only go in two cells, so other candidates in those cells can be removed)");
        result.put(sudokuStrategies::pointingPairsTriples, "pointing pairs/triples (candidate only appears in a specific row/column of a box, so that candidate can be removed from other boxes along that same line) ");
        result.put(sudokuStrategies::boxLineReduction, "box-line reduction (candidate only appears in a specific box of a row/column, so that candidate can be removed from other cells within that same box");

        return result;
    }

    /**
     * attempts to solve the board.  The board will be altered during solving, even if a solution is not found.
     * the @Handler and @XPath annotations are for camel.
     * They mean "prefer this function over other versions of solve()" and "Use this XML tag as the parameter", respectively
     *
     * @param toSolve Sudoku to solve.  The rules for this are the same as the constructor for SudokuBoard
     * @return whether or not solving was successful
     * @see SudokuBoard
     */
    @Handler
    public boolean solve(@XPath("Sudoku/Board") String toSolve)
    {
        return solve(new SudokuBoard(toSolve));
    }

    /**
     * performs a single iteration of solving logic on the given puzzle board.
     * in other words, it attempts every strategy until one makes progress.
     *
     * @param toSolve board to step
     * @return a ProposedChanges object with the resulting changes and a brief explanation string set for the strategy that made the changes.
     */
    @SuppressWarnings("unchecked")
    @Override
    public ProposedChangesWithCandidates<DigitCell, Byte> step(SudokuBoard toSolve)
    {
        //identical to the super in behavior, but explicitly states the actual return type that would result from a step on a sudoku board
        return (ProposedChangesWithCandidates<DigitCell, Byte>) super.step(toSolve);
    }
}