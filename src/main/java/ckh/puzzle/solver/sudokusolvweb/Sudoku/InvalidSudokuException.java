package ckh.puzzle.solver.sudokusolvweb.Sudoku;

import ckh.puzzle.solver.sudokusolvweb.common.Exceptions.InvalidPuzzleException;

public class InvalidSudokuException extends InvalidPuzzleException
{
    public InvalidSudokuException(String s)
    {
        super(s);
    }
}
