package ckh.puzzle.solver.sudokusolvweb.Sudoku.model;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.InvalidSudokuException;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.CellCoords;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.JsonDigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.PuzzleBoard;

import java.util.*;

/**
 * represents a sudoku board with all of the information needed to run solvers on it
 * @see JsonSudoku for the simplified representation used in Json files
 * @see XmlSudoku for the simplified representation used in XML files
 * @see DbSudoku for the simplified representation stored in the db
 */
public class SudokuBoard implements PuzzleBoard<SudokuGroup, DigitCell, Byte>
{
    //set of characters that have meaning in a sudoku
    private static final Set<Character> meaningfulCharacters = Collections.unmodifiableSet(
            new HashSet<>(Arrays.asList('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '*', '.', '?')));

    private DigitCell[][] boardContents;

    private SudokuGroup[] boardRows;
    private SudokuGroup[] boardColumns;
    private SudokuGroup[] boardBoxes;

    private SudokuGroup[] boardGroups;
    private List<DigitCell> boardContentsAsReadOnlyList;

    /**
     * constructs a blank board
     */
    public SudokuBoard()
    {
        boardContents = new DigitCell[9][9];
        for(DigitCell[] row : boardContents)
            for (DigitCell cell : row)
                cell = new DigitCell();

        buildGroups();
    }

    /**
     * constructs a sudoku board based on the input string
     * it will look for precisely 81 meaningful characters.  Meaningful characters include:
     * 1-9: fixed values that a cell should start with
     * '0', '*', '.', '?': represent a cell of unknown value
     * all other characters will be ignored.  This allows the input to be formatted in many ways and still construct a valid board.
     * @param contentString contents of the board
     * @throws InvalidSudokuException if the input could not be converted to a sudoku board.
     */
    public SudokuBoard(String contentString) throws InvalidSudokuException
    {
        //early error checking
        if (contentString == null || contentString.length() < 81)
            throw new InvalidSudokuException("input was null or did not have enough characters for this to be a sudoku.  Processing skipped.");

        char[] foundCharacters = new char[81];
        int count = 0;

        //for every character in the array
        for (char c : contentString.toCharArray())
        {
            //that has meaning
            if (meaningfulCharacters.contains(c))
            {
                //die if we found too many
                if (count == 81)
                    throw new InvalidSudokuException("input had too many meaningful characters for this to be a sudoku.");

                //otherwise, add it to the array
                foundCharacters[count++] = c;
            }
        }

        //die if we dont have enough
        if (count < 81)
            throw new InvalidSudokuException("input did not have enough meaningful characters to be a sudoku.");

        //we finally have an array of 81 meaningful characters!  Build our board.
        boardContents = new DigitCell[9][9];

        for (int r = 0; r < 9; r++)
        {
            for (int c = 0; c < 9; c++)
            {
                switch(foundCharacters[(r * 9) + c])
                {
                    case '1': boardContents[r][c] = new DigitCell((byte)1); break;
                    case '2': boardContents[r][c] = new DigitCell((byte)2); break;
                    case '3': boardContents[r][c] = new DigitCell((byte)3); break;
                    case '4': boardContents[r][c] = new DigitCell((byte)4); break;
                    case '5': boardContents[r][c] = new DigitCell((byte)5); break;
                    case '6': boardContents[r][c] = new DigitCell((byte)6); break;
                    case '7': boardContents[r][c] = new DigitCell((byte)7); break;
                    case '8': boardContents[r][c] = new DigitCell((byte)8); break;
                    case '9': boardContents[r][c] = new DigitCell((byte)9); break;
                    default:  boardContents[r][c] = new DigitCell();        break;
                }
            }
        }

        //and finally, build the group objects
        buildGroups();
    }

    /**
     * constructs a SudokuBoard to match the state of the given JsonSudoku
     *
     * @throws InvalidSudokuException if the board is not valid.
     * @see JsonSudoku
     * @param jsonSudoku the object to build from
     */
    public SudokuBoard(JsonSudoku jsonSudoku)
    {
        JsonDigitCell[][] jsonBoard = jsonSudoku.getBoard();

        //test row count
        if (jsonBoard.length != 9)
            throw new InvalidSudokuException("invalid JsonSudoku: incorrect number of rows!");

        //test column count
        for (int r = 0; r < 9; r++)
            if (jsonBoard[r].length != 9)
                throw new InvalidSudokuException("invalid JsonSudoku: row " + r + "has an incorrect number of cells!");

        //construct the cells
        boardContents = new DigitCell[9][9];
        for (int r = 0; r < 9; r++)
            for (int c = 0; c < 9; c++)
                boardContents[r][c] = new DigitCell(jsonBoard[r][c]);

        buildGroups();
    }

    /**
     * we will often need to refer to specific groups by row, column, or 3x3 box.
     * this function is a constructor helper that creates the SudokuGroup objects which will refer to these.
     */
    private void buildGroups()
    {
        //for the rows, we can just pass the arrays we already have
        boardRows = new SudokuGroup[]
                {
                    new SudokuGroup(boardContents[0]),
                    new SudokuGroup(boardContents[1]),
                    new SudokuGroup(boardContents[2]),
                    new SudokuGroup(boardContents[3]),
                    new SudokuGroup(boardContents[4]),
                    new SudokuGroup(boardContents[5]),
                    new SudokuGroup(boardContents[6]),
                    new SudokuGroup(boardContents[7]),
                    new SudokuGroup(boardContents[8])
                };

        //for columns, however, it will take a little more work
        boardColumns = new SudokuGroup[9];
        for (int c = 0; c < 9; c++)
        {
            boardColumns[c] = new SudokuGroup(new DigitCell[]
                    {
                            boardContents[0][c],
                            boardContents[1][c],
                            boardContents[2][c],
                            boardContents[3][c],
                            boardContents[4][c],
                            boardContents[5][c],
                            boardContents[6][c],
                            boardContents[7][c],
                            boardContents[8][c]
                    });
        }

        //and the boxes will take a little more than that
        boardBoxes = new SudokuGroup[9];
        for (int br = 0; br < 3; br++)
        {
            for (int bc = 0; bc < 3; bc++)
            {
                boardBoxes[(br * 3) + bc] = new SudokuGroup(new DigitCell[]
                        {
                           boardContents[(br * 3) + 0][(bc * 3) + 0],
                           boardContents[(br * 3) + 0][(bc * 3) + 1],
                           boardContents[(br * 3) + 0][(bc * 3) + 2],
                           boardContents[(br * 3) + 1][(bc * 3) + 0],
                           boardContents[(br * 3) + 1][(bc * 3) + 1],
                           boardContents[(br * 3) + 1][(bc * 3) + 2],
                           boardContents[(br * 3) + 2][(bc * 3) + 0],
                           boardContents[(br * 3) + 2][(bc * 3) + 1],
                           boardContents[(br * 3) + 2][(bc * 3) + 2]
                        });
            }
        }

        //and also store an array of all the groups we just made, so it doesnt have to be rebuilt for every call to getGroups()
        boardGroups = new SudokuGroup[]
                {
                        boardRows[0],
                        boardRows[1],
                        boardRows[2],
                        boardRows[3],
                        boardRows[4],
                        boardRows[5],
                        boardRows[6],
                        boardRows[7],
                        boardRows[8],
                        boardColumns[0],
                        boardColumns[1],
                        boardColumns[2],
                        boardColumns[3],
                        boardColumns[4],
                        boardColumns[5],
                        boardColumns[6],
                        boardColumns[7],
                        boardColumns[8],
                        boardBoxes[0],
                        boardBoxes[1],
                        boardBoxes[2],
                        boardBoxes[3],
                        boardBoxes[4],
                        boardBoxes[5],
                        boardBoxes[6],
                        boardBoxes[7],
                        boardBoxes[8]
                };


        List<DigitCell> cellList = new ArrayList<>(81);
        for (int r = 0; r < 9; r++)
            cellList.addAll(Arrays.asList(boardContents[r]));
        boardContentsAsReadOnlyList = Collections.unmodifiableList(cellList);
    }

    public SudokuGroup[] getRows()
    {
        return boardRows;
    }

    public SudokuGroup[] getColumns()
    {
        return boardColumns;
    }

    public SudokuGroup[] getBoxes()
    {
        return boardBoxes;
    }

    /**
     * @return a read-only list of cells in the board.  (the cells themselves can still be modified, just not the list)
     */
    @Override
    public List<DigitCell> getCells()
    {
        return boardContentsAsReadOnlyList;
    }

    /**
     * finds the given cell within this board
     *
     * @param cell cell to find
     * @return location of the given cell on the board, or null if it is not found
     */
    @Override
    public CellCoords findCell(DigitCell cell)
    {
        for (short r = 0; r < 9; r++)
            for (short c = 0; c < 9; c++)
                if (boardContents[r][c] == cell)
                    return new CellCoords(r, c);

        return null;
    }

    /**
     * returns a user-friendly string showing the contents of the sudoku board in a nice format
     */
    @Override
    public String toString()
    {
        //I apologize for this monstrosity of a statement.  Please forgive me.
        return "┌───┬───┬───┐\n" +
                "│" + boardContents[0][0] + boardContents[0][1] + boardContents[0][2] + "│" + boardContents[0][3] + boardContents[0][4] + boardContents[0][5] + "│" + boardContents[0][6] + boardContents[0][7] + boardContents[0][8] + "│\n" +
                "│" + boardContents[1][0] + boardContents[1][1] + boardContents[1][2] + "│" + boardContents[1][3] + boardContents[1][4] + boardContents[1][5] + "│" + boardContents[1][6] + boardContents[1][7] + boardContents[1][8] + "│\n" +
                "│" + boardContents[2][0] + boardContents[2][1] + boardContents[2][2] + "│" + boardContents[2][3] + boardContents[2][4] + boardContents[2][5] + "│" + boardContents[2][6] + boardContents[2][7] + boardContents[2][8] + "│\n" +
                "├───┼───┼───┤\n" +
                "│" + boardContents[3][0] + boardContents[3][1] + boardContents[3][2] + "│" + boardContents[3][3] + boardContents[3][4] + boardContents[3][5] + "│" + boardContents[3][6] + boardContents[3][7] + boardContents[3][8] + "│\n" +
                "│" + boardContents[4][0] + boardContents[4][1] + boardContents[4][2] + "│" + boardContents[4][3] + boardContents[4][4] + boardContents[4][5] + "│" + boardContents[4][6] + boardContents[4][7] + boardContents[4][8] + "│\n" +
                "│" + boardContents[5][0] + boardContents[5][1] + boardContents[5][2] + "│" + boardContents[5][3] + boardContents[5][4] + boardContents[5][5] + "│" + boardContents[5][6] + boardContents[5][7] + boardContents[5][8] + "│\n" +
                "├───┼───┼───┤\n" +
                "│" + boardContents[6][0] + boardContents[6][1] + boardContents[6][2] + "│" + boardContents[6][3] + boardContents[6][4] + boardContents[6][5] + "│" + boardContents[6][6] + boardContents[6][7] + boardContents[6][8] + "│\n" +
                "│" + boardContents[7][0] + boardContents[7][1] + boardContents[7][2] + "│" + boardContents[7][3] + boardContents[7][4] + boardContents[7][5] + "│" + boardContents[7][6] + boardContents[7][7] + boardContents[7][8] + "│\n" +
                "│" + boardContents[8][0] + boardContents[8][1] + boardContents[8][2] + "│" + boardContents[8][3] + boardContents[8][4] + boardContents[8][5] + "│" + boardContents[8][6] + boardContents[8][7] + boardContents[8][8] + "│\n" +
                "└───┴───┴───┘";
    }

    /**
     * prints the board with full debug info
     */
    public void debugPrint()
    {
        //if you thought toString() was bad, just wait 'till you see this...
        System.out.println("┌───┬───┬───┬┬───┬───┬───┬┬───┬───┬───┐"); //top border
        for (byte r = 0; r < 9; r++) //row on the board
        {
            for (byte rr = 0; rr < 3; rr++) //subrow of this row (each cell is rendered as its own 3x3 grid)
            {
                System.out.print("│"); //left border of table
                for (byte c = 0; c < 9; c++) //column on the board
                {
                    for (byte cc = 0; cc < 3; cc++) //subcolumn of this cell (each cell is rendered as its own 3x3 grid)
                    {
                        byte candidate = (byte)((rr * 3) + cc + 1);
                        System.out.print((boardContents[r][c].checkCandidate(candidate)) ? candidate : " ");
                    }

                    System.out.print("│"); //right border of cell

                    if ((c == 2) || (c == 5))
                        System.out.print("│"); //double border for 3x3 box
                }

                System.out.println(); //end of this subrow
            }

            switch(r)
            {
                case 2:
                case 5:
                    System.out.println("├───┼───┼───┼┼───┼───┼───┼┼───┼───┼───┤"); //double border for 3x3 box (falls through to default)
                default:
                    System.out.println("├───┼───┼───┼┼───┼───┼───┼┼───┼───┼───┤"); //normal bottom border
                    break;
                case 8:
                    System.out.println("└───┴───┴───┴┴───┴───┴───┴┴───┴───┴───┘"); //bottom of table
            }
        }
    }

    /**
     * gets all cell groups on the board
     * the meaning of this depends on the puzzle.
     * For example, on a Sudoku board each "group" is a row, column, or 3x3 box.
     * The point is a group should be a set of cells for SolverStrategy objects to work on
     *
     * @return a list containing the groups
     */
    @Override
    public SudokuGroup[] getGroups()
    {
        return boardGroups;
    }
}
