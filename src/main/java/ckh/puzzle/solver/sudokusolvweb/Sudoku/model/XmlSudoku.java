package ckh.puzzle.solver.sudokusolvweb.Sudoku.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * a sudoku as it is represented in XML files
 * @see DbSudoku for the simplified representation stored in the db
 * @see JsonSudoku for the simplified representation used in Json files
 * @see SudokuBoard for the representation that can be used to solve sudokus
 */
@XmlRootElement(name = "Sudoku")
public class XmlSudoku
{
    private String difficulty;
    private String name;
    private String boardString;

    @XmlElement(name = "Difficulty")
    public String getDifficulty()
    {
        return difficulty;
    }

    public void setDifficulty(String difficulty)
    {
        this.difficulty = difficulty;
    }

    @XmlElement(name = "Name")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @XmlElement(name = "Board")
    public String getBoardString()
    {
        return boardString;
    }

    public void setBoardString(String boardString)
    {
        this.boardString = boardString;
    }

    @Override
    public String toString()
    {
        return "XmlSudoku{" +
                "difficulty='" + difficulty + '\'' +
                ", name='" + name + '\'' +
                ", boardString='" + boardString + '\'' +
                '}';
    }
}
