package ckh.puzzle.solver.sudokusolvweb.Sudoku.model;

import ckh.puzzle.solver.sudokusolvweb.common.Difficulty;
import ckh.puzzle.solver.sudokusolvweb.dao.DifficultyDAO;
import ckh.puzzle.solver.sudokusolvweb.dao.SudokuDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.*;

/**
 * represents a sudoku as it is stored in the database.
 * @see JsonSudoku for the simplified representation used in Json files
 * @see XmlSudoku for the simplified representation used in XML files
 * @see SudokuBoard for the representation that can be used to solve sudokus
 */
@Entity
@Table(name = "PUZZLES_SUDOKU")
@Component
public class DbSudoku
{
    private DifficultyDAO difficultyDao;
    private SudokuDAO sudokuDAO;

    private Long id;
    private Difficulty difficulty;
    private String name;
    private String content;

    //to be used by spring for dependency injection
    @Autowired
    private void setDifficultyDao(DifficultyDAO DAO)
    {
        difficultyDao = DAO;
    }

    //to be used by spring for dependency injection
    @Autowired
    private void setSudokuDAO(SudokuDAO DAO)
    {
        sudokuDAO = DAO;
    }

    @Id
    @Column(name = "SUDOKU_ID")
    @SequenceGenerator(name = "generator", sequenceName = "SUDOKU_SEQ", allocationSize = 1)
    @GeneratedValue(generator = "generator")
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    @OneToOne()
    @JoinColumn(name = "DIFFICULTY_ID")
    public Difficulty getDifficulty()
    {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty)
    {
        this.difficulty = difficulty;
    }

    @Column(name = "SUDOKU_NAME")
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Column(name = "SUDOKU_CONTENT")
    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }

    //FUN STORY!  This produces a neatly aligned table, but it might not look that way
    //some monospace fonts dont properly handle the special characters used here
    //this seems to include the IntelliJ console font default, "monospaced"
    //(either that or there is some other weird thing happening I don't understand)
    //at any rate, this will look fine if you set said font to "Consolas".
    @Override
    public String toString()
    {
        return "DbSudoku:\n" +
                "id=" + id +
                ", difficulty=" + ((difficulty == null) ? "null" : difficulty.getName()) +
                ", name=" + name +
                ((content == null) ? " (NO CONTENT)" :
                    "\n┌───┬───┬───┐\n" +
                    "│" + content.substring( 0,  3) + "│" + content.substring( 3,  6) + "│" + content.substring( 6,  9) + "│\n" +
                    "│" + content.substring( 9, 12) + "│" + content.substring(12, 15) + "│" + content.substring(15, 18) + "│\n" +
                    "│" + content.substring(18, 21) + "│" + content.substring(21, 24) + "│" + content.substring(24, 27) + "│\n" +
                    "├───┼───┼───┤\n" +
                    "│" + content.substring(27, 30) + "│" + content.substring(30, 33) + "│" + content.substring(33, 36) + "│\n" +
                    "│" + content.substring(36, 39) + "│" + content.substring(39, 42) + "│" + content.substring(42, 45) + "│\n" +
                    "│" + content.substring(45, 48) + "│" + content.substring(48, 51) + "│" + content.substring(51, 54) + "│\n" +
                    "├───┼───┼───┤\n" +
                    "│" + content.substring(54, 57) + "│" + content.substring(57, 60) + "│" + content.substring(60, 63) + "│\n" +
                    "│" + content.substring(63, 66) + "│" + content.substring(66, 69) + "│" + content.substring(69, 72) + "│\n" +
                    "│" + content.substring(72, 75) + "│" + content.substring(75, 78) + "│" + content.substring(78, 81) + "│\n" +
                    "└───┴───┴───┘");
    }

    /**
     * converts an XmlSudoku object to a DbSudoku object
     * @param xmlSudoku original object
     * @return result object
     */
    public DbSudoku fromXml(XmlSudoku xmlSudoku)
    {
        DbSudoku result = sudokuDAO.findByName(xmlSudoku.getName());

        if (result == null)
        {
            result = new DbSudoku();
            result.setName(xmlSudoku.getName());
        }

        result.setDifficulty(difficultyDao.findByName(xmlSudoku.getDifficulty().toLowerCase()));
        result.setContent(xmlSudoku.getBoardString());
        return result;
    }
}
