package ckh.puzzle.solver.sudokusolvweb.Sudoku.model;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.InvalidSudokuException;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.JsonDigitCell;

import java.util.Arrays;

/**
 * represents a sudoku as it is stored in the database.
 * @see DbSudoku for the simplified representation used in the database
 * @see XmlSudoku for the simplified representation used in XML files
 * @see SudokuBoard for the representation that can be used to solve sudokus
 */
public class JsonSudoku
{
    private JsonDigitCell[][] board;

    public JsonDigitCell[][] getBoard()
    {
        return board;
    }

    public void setBoard(JsonDigitCell[][] board)
    {
        this.board = board;
    }

    @Override
    public String toString()
    {
        return "JsonSudoku{" +
                "board=" +
                Arrays.toString(board[0]) + ", " +
                Arrays.toString(board[1]) + ", " +
                Arrays.toString(board[2]) + ", " +
                Arrays.toString(board[3]) + ", " +
                Arrays.toString(board[4]) + ", " +
                Arrays.toString(board[5]) + ", " +
                Arrays.toString(board[6]) + ", " +
                Arrays.toString(board[7]) + ", " +
                Arrays.toString(board[8]) +
                '}';
    }

    /**
     * converts this object to a SudokuBoard object
     *
     * @throws InvalidSudokuException if the board is not valid.
     * @return this object as a SudokuBoard
     */
    public SudokuBoard toSudokuBoard()
    {
        return new SudokuBoard(this);
    }

    /**
     * creates an empty JsonSudoku
     */
    public JsonSudoku() { }

    /**
     * creates a JsonSudoku to represent the given sudokuBoard
     * @param sudokuBoard the object to convert
     */
    public JsonSudoku(SudokuBoard sudokuBoard)
    {
        board = new JsonDigitCell[9][9];

        for (int r = 0; r < 9; r++)
            for (int c = 0; c < 9; c++)
                board[r][c] = new JsonDigitCell(sudokuBoard.getRows()[r].getCells()[c]);
    }
}
