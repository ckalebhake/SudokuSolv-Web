package ckh.puzzle.solver.sudokusolvweb.dao;

import ckh.puzzle.solver.sudokusolvweb.common.Difficulty;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DifficultyDAO extends JpaRepository<Difficulty, Long>
{
    Difficulty findByName(String name);
}
