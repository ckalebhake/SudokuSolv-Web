package ckh.puzzle.solver.sudokusolvweb.dao;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.DbSudoku;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SudokuDAO extends JpaRepository<DbSudoku, Long>
{
    DbSudoku findByName(String name);
}
