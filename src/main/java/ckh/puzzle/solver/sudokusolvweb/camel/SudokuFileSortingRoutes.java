package ckh.puzzle.solver.sudokusolvweb.camel;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.DbSudoku;
import ckh.puzzle.solver.sudokusolvweb.dao.SudokuDAO;
import org.apache.camel.builder.RouteBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SudokuFileSortingRoutes extends RouteBuilder
{
    @Autowired private SudokuDAO sudokuDAO;

    @Override
    public void configure() throws Exception
    {
        //if anything in this route builder throws exceptions, reroutes the file to this folder and logs the exception
        errorHandler(deadLetterChannel("file:puzzleFiles/exception")
                .onExceptionOccurred(exchange -> Logger.getRootLogger().warn("Exception sorting Sudoku file: " + exchange.getException())));

        from("file:puzzleFiles/sudoku?noop=true")
                .choice()
                    .when(method("sudokuSolver", "solve"))
                    .to("file:puzzleFiles/solved")
                    .transform(method(DbSudoku.class, "fromXml"))
                    .to("jpa:DbSudoku")
                .otherwise()
                    .to("file:puzzleFiles/unsolved");
    }
}
