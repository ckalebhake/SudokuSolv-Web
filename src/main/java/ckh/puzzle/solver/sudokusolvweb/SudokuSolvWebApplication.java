package ckh.puzzle.solver.sudokusolvweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SudokuSolvWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SudokuSolvWebApplication.class, args);
	}
}
