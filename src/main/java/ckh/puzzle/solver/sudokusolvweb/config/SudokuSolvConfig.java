package ckh.puzzle.solver.sudokusolvweb.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties("SudokuSolv")
@Component
public class SudokuSolvConfig
{
    /**
     * whether or not to print the initial and final state of puzzles when a solver runs
     */
    private static boolean printOnSolve = false;

    /**
     * causes a full debug print of the puzzle board whenever a solving strategy proposes a change.
     * This allows for debugging solving logic.
     */
    private static boolean debugSolvers = false;

    /**
     * logs a warning whenever something uses the change justification "BECAUSE_I_SAID_SO"
     * @see ckh.puzzle.solver.sudokusolvweb.common.Solvers.ChangeProposalJustification.Reason
     */
    private static boolean warnUnjustifiedChanges = true;

    /**
     * throws exception whenever something uses the change justification "BECAUSE_I_SAID_SO"
     * @see ckh.puzzle.solver.sudokusolvweb.common.Solvers.ChangeProposalJustification.Reason
     */
    private static boolean errorUnjustifiedChanges = true;

    public static boolean shouldWarnUnjustifiedChanges()
    {
        return warnUnjustifiedChanges;
    }

    public static void setWarnUnjustifiedChanges(boolean newValue)
    {
        warnUnjustifiedChanges = newValue;
    }

    public static boolean shouldPrintOnSolve()
    {
        return printOnSolve;
    }

    public static void setPrintOnSolve(boolean newValue)
    {
        printOnSolve = newValue;
    }

    public static boolean shouldDebugSolvers()
    {
        return debugSolvers;
    }

    public static void setDebugSolvers(boolean newValue)
    {
        debugSolvers = newValue;
    }

    public static boolean shouldErrorUnjustifiedChanges()
    {
        return errorUnjustifiedChanges;
    }

    public static void setErrorUnjustifiedChanges(boolean errorUnjustifiedChanges)
    {
        SudokuSolvConfig.errorUnjustifiedChanges = errorUnjustifiedChanges;
    }
}
