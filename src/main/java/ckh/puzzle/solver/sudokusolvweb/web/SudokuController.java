package ckh.puzzle.solver.sudokusolvweb.web;

import ckh.puzzle.solver.sudokusolvweb.Sudoku.InvalidSudokuException;
import ckh.puzzle.solver.sudokusolvweb.Sudoku.SudokuSolver;
import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.JsonSudoku;
import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.JsonSudokuStepResult;
import ckh.puzzle.solver.sudokusolvweb.Sudoku.model.SudokuBoard;
import ckh.puzzle.solver.sudokusolvweb.common.Cells.DigitCell;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.JsonProposedChanges;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.JsonProposedChangesWithCandidates;
import ckh.puzzle.solver.sudokusolvweb.common.Solvers.ProposedChangesWithCandidates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/sudoku")
public class SudokuController
{
    private final SudokuSolver sudokuSolver;

    @Autowired
    public SudokuController(SudokuSolver sudokuSolver)
    {
        this.sudokuSolver = sudokuSolver;
    }

    @RequestMapping(path = "/solve", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity solveSudoku(@RequestBody JsonSudoku jsonSudoku)
    {
        SudokuBoard sudokuBoard;

        try
        {
            sudokuBoard = new SudokuBoard(jsonSudoku);
        }
        catch (InvalidSudokuException e)
        {
            return ResponseEntity.badRequest().body("failed to construct a board from the provided json.  Check that it is a 9x9 array of cell's candidates array is 9 booleans.");
        }

        if (!sudokuBoard.isValid())
        {
            System.out.println("invalid board!");
            System.out.println(sudokuBoard);
            return ResponseEntity.badRequest().body("The sudoku was constructed successfully, but the board given violates the rules of Sudoku.");
        }

        boolean result = sudokuSolver.solve(sudokuBoard);

        return ResponseEntity.ok(new JsonSudoku(sudokuBoard));
    }

    @RequestMapping(path = "/step", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity stepSudoku(@RequestBody JsonSudoku jsonSudoku)
    {
        SudokuBoard sudokuBoard;

        try
        {
            sudokuBoard = new SudokuBoard(jsonSudoku);
        }
        catch (InvalidSudokuException e)
        {
            return ResponseEntity.badRequest().body("failed to construct a board from the provided json.  Check that it is a 9x9 array of cell's candidates array is 9 booleans.");
        }

        if (!sudokuBoard.isValid())
        {
            System.out.println("invalid board!");
            System.out.println(sudokuBoard);
            return ResponseEntity.badRequest().body("The sudoku was constructed successfully, but the board given violates the rules of Sudoku.");
        }

        ProposedChangesWithCandidates<DigitCell, Byte> result = sudokuSolver.step(sudokuBoard);

        if ((result == null) || !result.hasChanges())
            return ResponseEntity.noContent().build();
        else
            return ResponseEntity.ok(new JsonSudokuStepResult(sudokuBoard, result));
    }
}
